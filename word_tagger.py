"""
spaCy NLP library used for word tokenization and pos tagging
"""

from spacy.lang.en import English
import os.path
import numpy as np
import print_progress as pp
import spacy as sp
import word_helper as wh


class WordTagger(object):
    """Support for common spaCy operations and model persistence"""
    def __init__(self, save_path, ner_tagger=False):
        self.save_path = save_path + 'tagger\\'
        self.pos_map = self.get_pos_map()
        nlp_handle = 'en_core_web_sm'
        if ner_tagger:
            self.nlp_model = sp.load(nlp_handle)
        else:
            self.nlp_model = sp.load(nlp_handle, disable=['parser', 'ner'])
        sbd_component = English().create_pipe('sentencizer')
        self.nlp_model.add_pipe(sbd_component, last=True)

    def __len__(self):
        return len(self.pos_map)

    def get_line_pos(self, line_str):
        line_items = []
        token_line = self.nlp_model(line_str)
        for token in token_line:
            pos_idx = self.pos_map[token.pos_]
            if not token.text.isspace():
                line_items.append((pos_idx, token.text))
        return line_items

    def get_line_tags(self, line_str):
        line_items = []
        token_line = self.nlp_model(line_str)
        for token_idx, token in enumerate(token_line):
            if len(token.ent_type_) > 0 and len(wh.norm_key(token.text)[0]) > 0:
                line_items.append((token_idx, token.text, token.ent_type_))
        return line_items

    def get_pos_map(self):
        dict_path = self.save_path + 'tag_map.json'
        pos_map = wh.load_json(dict_path)
        if pos_map is None:
            pos_idx, pos_map = 0, {}
            pos_keys = sp.parts_of_speech.IDS.keys()
            for k in sorted(pos_keys):
                pos_map[k] = pos_idx
                pos_idx += 1
            wh.save_json(dict_path, pos_map)
        return pos_map

    def get_vocab_vectors(self, key_list):
        rows, dims = self.nlp_model.vocab.vectors.shape
        vocab_vects = np.empty((len(key_list), dims))
        for k_idx in range(len(key_list)):
            k_id = self.nlp_model.vocab.strings[key_list[k_idx]]
            vocab_vects[k_idx] = self.nlp_model.vocab.vectors[k_id]
        return vocab_vects

    def tag_corpus(self, corpus_text):
        print("TAG_CORPUS: processing data...")
        pos_map = self.get_pos_map()
        tagged_text = []
        iter_count = 0
        for line in corpus_text:
            line_pos = self.get_line_pos(line)
            tagged_text.append(line_pos)
            iter_count += 1
            pp.print_progress(iter_count, len(corpus_text))
        print("\rTAG_CORPUS: operation complete.")
        return tagged_text

    def tag_dictionary(self, dict_obj):
        print("TAG_DICTIONARY: processing data...")
        pos_map = self.get_pos_map()
        iter_count = 0
        for k, v in dict_obj.items():
            dict_obj[k] = self.get_line_pos(v)
            iter_count += 1
            pp.print_progress(iter_count, len(dict_obj))
        print("\rTAG_DICTIONARY: operation complete.")

    def to_disk(self):
        print("TO_DISK: saving tagger...", end='', flush=True)
        os.makedirs(os.path.dirname(self.save_path), exist_ok=True)
        self.nlp_model.to_disk(self.save_path)
        print("\rTO_DISK: operation complete.")