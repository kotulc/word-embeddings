"""
Collection of one-to-one key-value maps and one-to-many dictionaries
for use in vocabulary reducation and generalization
"""

import operator
import string
import word_helper as wh
import word_io as io
import word_strings as wstr
import word_subsets as ws
import word_wordnet as wn


# WordNet Part-of-speech constants
ADJ, ADJ_SAT, ADV, NOUN, VERB = 'a', 's', 'r', 'n', 'v'
MAPS_PATH = wh.SAVE_PATH + 'maps\\'


def get_definition_dict(word_list):
    """Return a simplified dictionary in the form word_key: def_str"""
    data_path, simple_dict = 'maps\\def_dict.dat', {}
    def_dict = io.load_or_build(wn.wordnet_definitions, [word_list], data_path)
    for k, v in def_dict.items():
        simple_str = ''
        for def_pos, def_str in v:
            simple_str += def_str + ' '
        simple_str = wh.norm_string(simple_str)
        simple_dict[k] = [k.lower() for k in simple_str.split()]
    return simple_dict


def get_key_counts(count_dict, key_list):
    """Extract word counts for shared corpus and dictionary words"""
    for w in key_list:
        w = w.lower()
        if w in count_dict:
            count_dict[w] += 1
        else:
            count_dict[w] = 1
    return count_dict


def get_root_map():
    """Return a dictionary mapping word keys to lists of root keys"""
    data_path = 'maps\\root_map.dat'
    return io.load_or_build(root_map, [], data_path)


def get_tag_map(key_set):
    """Return a class_map, key_map pair mapping tag-key relationships"""
    data_path = 'maps\\tag_map.dat'
    return io.load_or_build(tag_map, [key_set], data_path)


def get_token_map(key_set, key_tokens=8, token_classes=128):
    """Return a class_map, key_map pair mapping token-key relationships"""
    data_path = 'maps\\token_map.dat'
    fcn_args = [key_set, key_tokens, token_classes]
    return io.load_or_build(token_map, fcn_args, data_path)


def match_substring(key_dict, key_str):
    """Return a matching prefix or suffix pair of key_str"""
    k_prefix, k_suffix = key_str, key_str
    while len(k_prefix) > 2:
        k_prefix, k_suffix = k_prefix[1:], k_suffix[:-1]
        if k_prefix in key_dict:
            return k_prefix
        elif k_suffix in key_dict:
            return k_suffix
    return None


def match_substrings(key_set, key_str):
    """Return a matching prefix or suffix pair of key_str"""
    substring_list = []
    k_prefix, k_suffix = key_str, key_str
    while len(k_prefix) > 2:
        k_prefix, k_suffix = k_prefix[1:], k_suffix[:-1]
        if k_prefix in key_set:
            substring_list.append(k_prefix)
        elif k_suffix in key_set:
            substring_list.append(k_suffix)
    return substring_list


def root_map():
    """Return a dictionary mapping word keys to lists of root keys"""
    def_dict, root_map = ws.get_all_definitions(), {}
    count_dict, key_idx, key_set = {}, 0, set(ws.get_common_keys())
    for k, v in def_dict.items():  # Collect definition key counts
        get_key_counts(count_dict, v.split())
    count_dict = {k: v for k, v in count_dict.items() if k in key_set}
    op_key = operator.itemgetter(1)
    for w_key, def_str in def_dict.items():
        if not wstr.match_numeric(w_key):
            if w_key in key_set:
                root_map[w_key] = w_key
            else:
                def_keys = [k for k in def_str.split() if k in key_set]
                if len(def_keys) == 0:
                    root_map[w_key] = w_key
                else:
                    key_dict = {k: count_dict[k] for k in def_keys}
                    k_sorted = sorted(key_dict.items(), key=op_key, reverse=False)
                    root_map[w_key] = k_sorted[0][0]
    return root_map


def tag_map(key_set):
    """Return a class_map, key_map pair mapping tag-key relationships"""
    class_idx, class_map, key_map = 1, {}, {}
    data_path = 'wnet\\attr_dict.dat'
    attr_dict = io.load_or_build(wn.wordnet_attributes, [key_set], data_path)
    for pos_key in wn.POS_KEYS:
        class_map[pos_key] = class_idx
        class_idx += 1
    for attr_key in wn.ATTR_KEYS:
        class_map[attr_key] = class_idx
        class_idx += 1
    class_map['char'] = class_idx
    class_map['loc'] = class_idx + 1
    class_map['name'] = class_idx + 2
    loc_keys = wh.load_json(wh.SAVE_PATH + 'lists\\places.json')
    loc_keys = {k.lower() for k in loc_keys}
    name_keys = wh.load_json(wh.SAVE_PATH + 'lists\\names.json')
    name_keys = {k.lower() for k in name_keys}
    for w_key, (pos_keys, attr_keys) in attr_dict.items():
        key_map[w_key] = [class_map[k] for k in pos_keys]
        key_map[w_key].extend([class_map[k] for k in attr_keys])
        if len(w_key) == 1:
            key_map[w_key].append(class_map['char'])
        if w_key in loc_keys:
            key_map[w_key].append(class_map['loc'])
        if w_key in name_keys:
            key_map[w_key].append(class_map['name'])
    for k, v in key_map.items():
        if len(v) == 0:
            key_map[k] = [class_idx + 3]
    class_map = {v: k for k, v in class_map.items()}  # Reverse map for lookup
    return class_map, key_map


def token_map(key_set, key_tokens, token_classes):
    """Return a class_map, key_map pair mapping token-key relationships"""
    class_idx, key_map = 1, {}  # In the form of word_key: [class_idx, ...]
    # Collect a list of token_classes most common keys from def_dict values
    count_dict, def_dict = {}, ws.get_all_definitions()
    def_dict = {k: v.split() for k, v in def_dict.items()}
    for w_key, def_keys in def_dict.items():  # Collect definition key counts
        get_key_counts(count_dict, def_keys)
    count_dict = {k: v for k, v in count_dict.items() if k in key_set}
    op_key = operator.itemgetter(1)  # Sort most common definition str keys
    c_sorted = sorted(count_dict.items(), key=op_key, reverse=True)
    class_map = {k: 1 for k, v in c_sorted[:token_classes]}  # Retain top n keys
    for w_key in class_map.keys():  # Assign each token a unique class index
        class_map[w_key] = class_idx
        class_idx += 1
    for w_key in key_set:  # Map keys to token classes
        if w_key not in def_dict:
            key_map[w_key] = [token_classes]  # Assign null class
        else:
            def_keys = def_dict[w_key]
            key_dict = {k: count_dict[k] for k in def_keys if k in class_map}
            if len(key_dict) == 0:  # No matching tokens
                key_map[w_key] = [token_classes]  # Assign null class
            else:  # Assign all token class indices
                k_sorted = sorted(key_dict.items(), key=op_key, reverse=False)
                key_map[w_key] = [class_map[k] for k, v in k_sorted[:key_tokens]]
    class_map = {v: k for k, v in class_map.items()}  # Reverse map for lookup
    return class_map, key_map


########################  WORD_MAPS DEPRECATED LOGIC  ########################


def add_tags(dict_obj, dict_val, key_list):
    """Add all word_key: dict_val pair from key_list"""
    if key_list is None:
        raise FileNotFoundError('ADD_TAG_LIST: missing key list.')
    for w_key in key_list:
        dict_obj[w_key] = dict_val
    return dict_obj


def get_common_relation(count_dict, word_key):
    """Return related key more common then word_key"""
    k_count = count_dict[word_key]
    k_list = []
    rel_list = wn.wordnet_relation_list(word_key, rel_type='hyper')
    for rel_key in rel_list:
        k_list.extend(wh.simplify_key(rel_key))
    k_list = [k for k in k_list if k in count_dict and count_dict[k] > k_count]
    if len(k_list) == 0:
        rel_list = wn.wordnet_relation_list(word_key, rel_type='all')
        for rel_key in rel_list:
            k_list.extend(wh.simplify_key(rel_key))
        k_list = [k for k in k_list if k in count_dict and count_dict[k] > k_count]
        if len(k_list) == 0:
            return None
    min_count, min_key = count_dict[k_list[0]], k_list[0]
    for k in k_list[1:]:
        if count_dict[k] < min_count:
            min_count, min_key = count_dict[k], k
    return min_key


def key_common_map(root_count):
    dict_path = MAPS_PATH + 'common_map.json'
    c_map = wh.load_json(dict_path)
    if c_map is None:
        wnet_keys = ws.get_wnet_vocab()
        count_dict = ws.get_common_dict(wnet_keys)
        op_key = operator.itemgetter(1)
        c_sorted = sorted(count_dict.items(), key=op_key, reverse=True)
        # Account for 58 alpha and punctuation characters
        root_set = {k for k, v in c_sorted[:root_count - 58]}
        c_map = key_hyper_map(count_dict)
        for k in c_map.keys():
            if len(c_map[k]) == 0 or k in root_set:
                c_map[k] = k
        remap_list = [k for k in c_map.keys() if k not in root_set]
        for k in remap_list:
            # Traverse parent hierarchy until common_set entry selected
            key_stack, parent_key = set(), c_map[k]
            while parent_key not in root_set and parent_key not in key_stack:
                key_stack.add(parent_key)
                parent_key = c_map[parent_key]
            c_map[k] = parent_key
        #for k in string.ascii_lowercase + string.punctuation:
        for k in string.ascii_lowercase:
            c_map[k] = k  # Map lower case characters
        wh.save_json(dict_path, c_map)
    return c_map


def key_default_maps(token_target):
    """Return a dictionary mapping (tag, token): [word_key,...]"""
    locations = wh.load_json(wh.SAVE_PATH + 'lists\\places.json')
    names = wh.load_json(wh.SAVE_PATH + 'lists\\names.json')
    if locations is None or names is None:
        raise FileNotFoundError('KEY_DEFAULT_MAPS: missing name/place file.')
    #chars, punct = string.ascii_lowercase, string.punctuation
    chars = string.ascii_lowercase
    # Build the full tag-token-key map in the form word_key: (tag, token)
    token_map = key_common_map(token_target)  # word_key: word_token
    tag_map = key_pos_map(token_map.keys())  # word_key: pos_tag
    update_key_maps('<CHAR>', tag_map, 'character', token_map, chars)
    update_key_maps('<LOC>', tag_map, 'location', token_map, locations)
    update_key_maps('<PER>', tag_map, 'name', token_map, names)
    #update_key_maps('<PUNCT>', tag_map, 'punctuation', token_map, punct)
    return tag_map, token_map


def key_hyper_map(count_dict):
    """Return a dictionary mapping keys to their common root keys"""
    dict_path = MAPS_PATH + 'hyper_map.json'
    hyper_map = wh.load_json(dict_path)
    if hyper_map is None:
        hyper_map = {k: '' for k in count_dict.keys()}
        for k in hyper_map.keys():
            rel_key = get_common_relation(count_dict, k)
            if rel_key is None:
                rel_key = match_substring(hyper_map, k)
            hyper_map[k] = rel_key
        wh.save_json(dict_path, hyper_map)
    return hyper_map


def key_pos_map(word_list):
    """Return a dictionary with items in the form of word_key: pos_token"""
    dict_path = MAPS_PATH + 'tag_map.json'
    pos_map = wh.load_json(dict_path)
    if pos_map is None:
        pos_map = {}
        for k in word_list:
            pos_list = wn.wordnet_pos_list(k)
            if len(pos_list) == 0:
                pos_map[k] = '<POS-NULL>'
            else:
                pos_list.sort()
                pos_str = '<POS-'
                for p_key in pos_list:
                    pos_str += p_key[0].upper()
                pos_map[k] = pos_str + '>'
        wh.save_json(dict_path, pos_map)
    return pos_map


def key_rel_map(word_list, relation_keys=32):
    """Return a dictionary in the form word_key: [rel_key, ...]"""
    count_dict = ws.get_common_dict(word_list)
    rel_map = {}
    for word_key in word_list:
        k_list = []
        rel_list = wn.wordnet_relation_list(word_key, rel_type='all')
        for rel_key in rel_list:
            k_list.extend(wh.simplify_key(rel_key))
        k_list = list(set(k_list))
        # Tile keys if len(k_list) < relation_keys
        # Crop keys else
    return rel_map


def key_supstr_map():
    """Return a dictionary containing super:sub_str maps of common words"""
    wnet_keys = ws.get_wnet_vocab()
    substr_dict = substr_key_dict()
    supstr_dict = {k: '' for k in wnet_keys}
    for k, v in substr_dict.items():
        for v_str in v:
            if len(supstr_dict[v_str]) < len(k):
                supstr_dict[v_str] = k
    return supstr_dict


def substr_key_dict():
    """Return a dictionary containing substrings (>=1 char) of common words"""
    dict_path = MAPS_PATH + 'substr_map.json'
    substr_dict = wh.load_json(dict_path)
    if substr_dict is None:
        wnet_keys = ws.get_wnet_vocab()
        count_dict = ws.get_count_dict(wnet_keys)
        common_keys = {k for k in count_dict.keys() if count_dict[k] > 1}
        substr_dict = {}
        for k in wnet_keys:
            k_pfx, k_sfx = k, k
            while len(k_pfx) >= 2:
                wh.dict_add(substr_dict, k_pfx[1:], k)
                wh.dict_add(substr_dict, k_sfx[:-1], k)
                k_pfx, k_sfx = k_pfx[1:], k_sfx[:-1]
        # Remove keys where len(val)==1 and key not in common_keys
        substr_dict = {k: list(v) for k, v in substr_dict.items()}
        for k in list(substr_dict.keys()):
            if k not in common_keys and len(substr_dict[k]) == 1:
                del substr_dict[k]
            elif k not in common_keys:  # If all values share a longer substring key...
                pfx_flag, sfx_flag = True, True
                v = substr_dict[k][0]
                pfx_str = v[:len(k) + 1]
                sfx_str = v[(len(v) - len(k)) - 1:]
                for v in substr_dict[k]:
                    pfx_flag *= v[:len(k) + 1] == pfx_str
                    sfx_flag *= v[(len(v) - len(k)) - 1:] == sfx_str
                if pfx_flag or sfx_flag:
                    del substr_dict[k]  # delete this key
        wh.save_json(dict_path, substr_dict)
    return substr_dict


def update_key_maps(tag_key, tag_map, token_key, token_map, word_list):
    """Assign tag_key and token_key to all word keys in word_list"""
    for w in word_list:
        tag_map[w.lower()] = tag_key
        token_map[w.lower()] = token_key
    return tag_map, token_map