"""
Common dictionary processing, formatting and related subroutines
"""

import word_strings as ws


def append_string(dict_key, dict_obj, string_value):
    """Append or initialize string_value to the dict_key entry in dict_obj"""
    if dict_key not in dict_obj:
        dict_obj[dict_key] = string_value
    else:
        dict_obj[dict_key] += ' ' + string_value


def merge_string_dictionaries(left_dict, right_dict):
    """Merge two dictionaries; append string values for matching keys"""
    merged_dict = {}
    for k, v in left_dict.items():
        append_string(k, merged_dict, v)
    for k, v in right_dict.items():
        append_string(k, merged_dict, v)
    return merged_dict


def simplify_string_dictionary(dict_obj):
    """Return a string dict with single word keys and normalized values"""
    simple_dict = {}
    for k, v in dict_obj.items():
        key_list = ws.simplify_key(k)
        norm_str = ws.norm_string(v)
        for sub_key in key_list:
            append_string(sub_key, simple_dict, norm_str)
    return simple_dict
