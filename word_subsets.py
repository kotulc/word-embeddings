"""
Data access layer for word modules; responsible for extracting/loading,
structuring, saving and providing access to word relation sub-sets.
Used primary by word_tree.py and word_encoder.py
"""

import itertools
import nltk.corpus
import print_progress as pp
import word_dict as wd
import word_helper as wh
import word_io as io
import word_strings as ws
import word_tagger as wt
import word_wordnet as wn


# WordNet Part-of-speech constants
ADJ, ADJ_SAT, ADV, NOUN, VERB = 'a', 's', 'r', 'n', 'v'

# Word Part-of-speech token map
POS_MAP = {ADJ:'<ADJ>', ADJ_SAT:'<ADJ>', ADV:'<ADV>', NOUN:'<NOU>', VERB:'<VRB>'}

ALL_SUFFIX = ['ative','itive','ation','ition','ical','tion','able','ible',
              'less','ment','ness','eous','ious','ize','ism','ial','ful','ing',
              'ion','ity','ous','ism','ive','est','al','ed','en','er','ic',
              'es','ly','ty','y','s','a','e','d']
NEG_PREFIX = ['un','in','im','il','ir','de','ex','sub','non','mis',
              'mal','dis','infra','under','mini','semi','anti','quasi']
NEU_PREFIX = ['bi','pre','tri','hex','oct','dec','inter','super','ante',
              'peri','ante','semi','mono','quad','penta','sept','trans']
POS_PREFIX = ['re','co','pro','pan','con','poly','over','equi','micro',
              'macro','mega','extra','prime','post','retro','multi','auto']

ALL_PREFIX = NEG_PREFIX + NEU_PREFIX + POS_PREFIX
SFX_SORTED = sorted(ALL_SUFFIX, key=len)
PFX_SORTED = sorted(ALL_PREFIX, key=len)

WNET_PATH = wh.SAVE_PATH + 'wnet\\'


def _build_count_dict(word_list):
    """Extract sample word_list counts over sample corpus data
    :param dict_path: file path to save/load count_dict if available
    :param word_list: the list of words to collect counts for
    :return: a dictionary of word_list counts (min==1)"""
    count_dict = {k:1 for k in word_list}
    _extract_counts(get_sample_corpus(), count_dict)
    # Use NLTK corpus in addition to the sample_corpus
    _extract_counts(nltk.corpus.genesis.words(), count_dict)
    _extract_counts(nltk.corpus.inaugural.words(), count_dict)
    _extract_counts(nltk.corpus.state_union.words(), count_dict)
    _extract_counts(nltk.corpus.webtext.words(), count_dict)
    return count_dict


def _extract_counts(word_corpus, word_dict):
    """Extract word counts for shared corpus and dictionary words"""
    for w in word_corpus:
        w = w.lower()
        if w in word_dict:
            word_dict[w] += 1
        else:
            word_dict[w] = 1
    return word_dict


def get_common_dict(word_list):
    """Return a count dictionary of common words including word_list keys"""
    count_dict = get_count_dict(word_list)
    # Remove special characters, numerics and spaces
    for k in list(count_dict.keys()):
        k_val = count_dict[k]
        del count_dict[k]  # Remove non-normalized key
        # Remove special characters and spaces
        norm_k = wh.norm_key(k)[0]
        # Remove unicode characters and digits
        norm_k = wh.trim_unicode(norm_k)
        norm_k = wh.trim_numerics(norm_k)
        if not norm_k.isspace() and len(norm_k) > 0:
            if norm_k in count_dict:
                count_dict[norm_k] += k_val
            else:
                count_dict[norm_k] = k_val
    return count_dict


def get_common_keys():
    """Return the list of 10k common keys from the Google english list"""
    key_list = io.load_text(io.SAVE_PATH + 'lists\\google_10k.txt')
    return [k.rstrip() for k in key_list]  # Remove trailing newline


def get_count_dict(word_list):
    dict_path = WNET_PATH + 'wnet_count.json'
    print("GET_COUNT_DICT: processing corpora...", end='', flush=True)
    count_dict = wh.load_json(dict_path)
    if count_dict is None:
        # Extracts all common word_list values from count_dict
        count_dict = _build_count_dict(word_list)
        wh.save_json(dict_path, count_dict)
    print("\rGET_COUNT_DICT: operation complete.")
    return count_dict


def get_data_dict(data_path, word_dict):
    """Tag all value strings in word_dict with part-of-speech indices"""
    data_dict = wh.load_pickle(data_path)
    if data_dict is None:
        pos_tagger = wt.WordTagger(wh.SAVE_PATH)
        out_str = "GET_DATA_DICT: tagging, iteration ="
        data_dict = {k: [] for k in word_dict.keys()}
        for iter_count, (k, v) in enumerate(word_dict.items()):
            for line_pos, line_str in v:
                norm_line = wh.norm_string(line_str)
                tagged_list = pos_tagger.get_line_pos(norm_line)
                tagged_list.insert(0, line_pos)
                data_dict[k].append(tagged_list)
            pp.print_iterations(iter_count, out_str)
        wh.save_pickle(data_path, data_dict)
    print("\rGET_DATA_DICT: operation complete.")
    return data_dict


def get_all_definitions():
    """Return a dictionary of combined Webster's and Wordnet definitions"""
    data_path, simple_dict = 'wnet\\def_dict.dat', {}
    wnet_defs = io.load_or_build(wn.wordnet_definitions, [None, True], data_path)
    wnet_defs = wd.simplify_string_dictionary(wnet_defs)
    webs_defs = io.load_json(wh.SAVE_PATH + 'lists\\dictionary.json')
    webs_defs = wd.simplify_string_dictionary(webs_defs)
    return wd.merge_string_dictionaries(wnet_defs, webs_defs)


def get_sample_corpus():
    # Extract a list of lines from a Gutenberg EBook
    sample_doc = open(wh.SAVE_PATH + 'lists\\big.txt', 'r')
    sample_lines = [wh.tokenize_norm(line) for line in sample_doc.readlines()]
    sample_corpus = list(itertools.chain.from_iterable(sample_lines))
    return sample_corpus


def get_wnet_definitions(tagged_tokens=False, word_list=None):
    """Load or extract word definition dictionary for all word_list keys"""
    data_path = WNET_PATH + 'wnet_defs.dat'
    json_path = WNET_PATH + 'wnet_defs.json'
    def_dict = wh.load_pickle(data_path)
    if def_dict is None:
        print("GET_WNET_DEFS: collecting data...", end='', flush=True)
        def_dict = wh.load_json(json_path)
        if def_dict is None:
            if word_list is None:
                word_list = get_wnet_vocab()
            def_dict = wn.wordnet_definitions(word_list)
            wh.save_json(json_path, def_dict)
        if tagged_tokens:
            def_dict = get_data_dict(data_path, def_dict)
        print("\rGET_WNET_DEFS: operation complete.")
    return def_dict


def get_wnet_examples(tagged_tokens=False, word_list=None):
    """Load or extract word examples for all word_list keys"""
    data_path = WNET_PATH + 'wnet_exps.dat'
    json_path = WNET_PATH + 'wnet_exps.json'
    exp_dict = wh.load_pickle(data_path)
    if exp_dict is None:
        print("\rGET_WNET_EXPS: collecting data...")
        exp_dict = wh.load_json(json_path)
        if exp_dict is None:
            if word_list is None:
                word_list = get_wnet_vocab()
            exp_dict = wn.wordnet_examples(word_list)
            wh.save_json(json_path, exp_dict)
        if tagged_tokens:
            exp_dict = get_data_dict(data_path, exp_dict)
        print("\rGET_WNET_EXPS: operation complete.")
    return exp_dict


def get_wnet_vocab():
    list_path = WNET_PATH + 'wnet_vocab.json'
    print("GET_WNET_VOCAB: collecting data...", end='', flush=True)
    vocab_list = wh.load_json(list_path)
    if vocab_list is None:
        vocab_list = []
        wnet_list = wn.wordnet_words()
        for w in wnet_list:
            s_keys = wh.simplify_key(w)
            vocab_list.extend(s_keys)
        # Reduce list to only unique word keys
        vocab_list = list(set(vocab_list))
        vocab_list = [k for k in vocab_list if not wh.match_numeric(k)]
        wh.save_json(list_path, vocab_list)
    print("\rGET_WNET_VOCAB: operation complete.")
    return vocab_list


########################  DEPRECATED LOGIC  ########################


def get_expansion_dicts(word_list, word_pos):
    """Load or generate dictionaries of words related to those in word_list
    :param word_list: List of words to expand if no saved dict. is found
    :param word_pos: word part of speech to extract from WordNet
    :return: List of dictionaries mapping word_lists words to related words"""
    dict_prefix = 'wnet_' + word_pos + '_'
    print("GET_EXPANSION_DICTS: collecting data...", end='', flush=True)
    word_data = {}
    for r_type in wn.REL_TYPES[2:]:
        dict_path = WNET_PATH + dict_prefix + r_type + '.json'
        w_dict = wh.load_or_build(
            dict_path, expansion_dict, [word_list, word_pos, r_type]
        )
        word_data[r_type] = w_dict
    print("\rGET_EXPANSION_DICTS: operation complete.")
    return word_data


def expansion_dict(word_list, word_pos, word_rel):
    """Load or generate a dict. of words related to those in word_list"""
    exp_dict = {w:[] for w in word_list}
    print("EXPANSION_DICT: expanding %s" % word_rel + word_pos)
    exp_dict = wn.wordnet_expand(word_list, exp_dict, rel_type=word_rel)
    exp_dict = simplify_dict(exp_dict)
    return exp_dict


def get_expanded_set(key_set, word_dict):
    # Return the expanded set of words from word_dict values
    return {w for k in key_set for w in word_dict[k]}


def get_prefix_map(word_key):
    """Return the matching prefix affiliation"""
    if wh.match_prefix(word_key, NEU_PREFIX) is not None:
        return '<NEU>'
    elif wh.match_prefix(word_key, POS_PREFIX) is not None:
        return '<POS>'
    elif wh.match_prefix(word_key, NEG_PREFIX) is not None:
        return '<NEG>'
    return None


def get_relation_dict(word_pos, rel_type='all'):
    """Return a WordNet dictionary mapping pos words to rel_type"""
    dict_path = WNET_PATH + 'wnet_' + word_pos + '_' + rel_type + '.json'
    word_dict = wh.load_json(dict_path)
    if word_dict is None:
        if rel_type == 'all':
            word_dict = {}
            for r_type in wn.REL_TYPES[2:]:
                word_dict = wn.wordnet_extract(word_pos, word_dict, r_type)
        else:
            word_dict = wn.wordnet_extract(word_pos, rel_type=rel_type)
        word_dict = simplify_dict(word_dict)
        wh.save_json(dict_path, word_dict)
    return word_dict


def get_shared_subsets(source_set, split_set):
    """Compare source_set and split_set, return shared values as shared_subset"""
    shared_subset = split_set.intersection(source_set)
    split_subset = split_set.difference(shared_subset)
    return shared_subset, split_subset


def get_substring_map(word_set, word_key):
    """Return the closest matching substring from word_set"""
    sfx_val = wh.match_suffix(word_key, SFX_SORTED)
    if sfx_val is not None:
        if word_key[:-1 * len(sfx_val)] in word_set:
            return word_key[:-1 * len(sfx_val)]
    pfx_val = wh.match_prefix(word_key, PFX_SORTED)
    if pfx_val is not None:
        if word_key[len(pfx_val):] in word_set:
            return word_key[len(pfx_val):]
    for char_idx in range(1, len(word_key)//2):
        if word_key[:-1 * char_idx] in word_set:
            return word_key[:-1 * char_idx]
        if word_key[char_idx:] in word_set:
            return word_key[char_idx:]
    return None


def get_wnet_adjectives(word_list):
    list_path = WNET_PATH + 'wnet_adj.json'
    print("GET_WNET_ADJECTIVES: collecting data...", end='', flush=True)
    adj_list = wh.load_json(list_path)
    if adj_list is None:
        adj_dict = wn.wordnet_extract(ADJ)
        adjs_dict = wn.wordnet_extract(ADJ_SAT)
        adj_dict = wh.dict_merge(adj_dict, adjs_dict)
        adj_dict = simplify_dict(adj_dict)
        adj_list = trim_list(adj_dict, word_list)
        wh.save_json(list_path, adj_list)
    print("\rGET_WNET_ADJECTIVES: operation complete.")
    return 'adjective', adj_list


def get_wnet_adverbs(word_list):
    list_path = WNET_PATH + 'wnet_adv.json'
    print("GET_WNET_ADVERBS: collecting data...", end='', flush=True)
    adv_list = wh.load_json(list_path)
    if adv_list is None:
        adv_dict = wn.wordnet_extract(ADV)
        adv_dict = simplify_dict(adv_dict)
        adv_list = trim_list(adv_dict, word_list)
        wh.save_json(list_path, adv_list)
    print("\rGET_WNET_ADVERBS: operation complete.")
    return 'adverb', adv_list


def get_wnet_nouns(key_list):
    """Extract noun hierarchy from WordNet; merging relation dictionaries
    :return: the root word of the hierarchy and the associated rel. dict."""
    dict_path = WNET_PATH + 'wnet_nouns.json'
    root_key = 'entity'
    print("GET_WNET_NOUNS: collecting data...", end='', flush=True)
    hypo_dict = wh.load_json(dict_path)
    if hypo_dict is None:
        hyper_dict = get_relation_dict('n', rel_type='hypr')
        hypo_dict = get_relation_dict('n', rel_type='hypo')
        hypo_dict = merge_hyp_dicts(hypo_dict, hyper_dict)
        hypo_dict = trim_hierarchy(hypo_dict, key_list, root_key)
        wh.save_json(dict_path, hypo_dict)
    print("\rGET_WNET_NOUNS: operation complete.")
    return root_key, hypo_dict


def get_wnet_verbs(key_list):
    """Extract verb hierarchy from wordnet; merge hier. rel. dicts
    :return: the verb parent:child_nodes hierarchy dictionary"""
    dict_path = WNET_PATH + 'wnet_verbs.json'
    root_key = 'verb'
    print("GET_WNET_VERBS: collecting data...", end='', flush=True)
    hypo_dict = wh.load_json(dict_path)
    if hypo_dict is None:
        hyper_dict = get_relation_dict('v', rel_type='hypr')
        hypo_dict = get_relation_dict('v', rel_type='hypo')
        hypo_dict = merge_hyp_dicts(hypo_dict, hyper_dict)
        word_roots = [k for k, v in hyper_dict.items() if len(v) == 0]
        hypo_dict['verb'] = word_roots
        hypo_dict = trim_hierarchy(hypo_dict, key_list, root_key)
        wh.save_json(dict_path, hypo_dict)
    print("\rGET_WNET_VERBS: operation complete.")
    return root_key, hypo_dict


def get_word_mapping(anchor_set, word_dict):
    """Pipeline for matching dictionary keys and values to anchor_set values
    :param anchor_set: set of words to match with word_dict keys
    :param word_dict: key:word_list, mapping parent word to child words
    :return: list of (key, list) pairings ordered by depth"""
    key_list_pairs = []
    # Set of word keys shared between word_set and anchored words
    anchor_keys = anchor_set
    # Set of remaining words to anchor
    word_set = set(word_dict.keys())
    while len(anchor_keys) > 0 and len(word_set) > 0:
        # Split word_set into shared and remaining word subsets
        shared_set, word_set = get_shared_subsets(anchor_keys, word_set)
        # Add all shared (now anchored) words to the list of pairs
        kl_pairs = [(k, word_dict[k]) for k in shared_set]
        key_list_pairs.extend(kl_pairs)
        # Build a new set of keys to match against word_set
        anchor_keys = get_expanded_set(shared_set, word_dict)
        anchor_set = anchor_set.union(anchor_keys)
    # For all remaining words, attempt to map to a substring anchor; else None
    none_count = 0
    for word in word_set:
        w_key = None
        w_list = [word] + word_dict[word]
        for w in w_list:
            w_key = match_word_set(w, anchor_set)
            if w_key is not None:
                break
        if w_key is None: none_count += 1
        key_list_pairs.append((w_key, w_list))
        anchor_set = anchor_set.union(w_list)
    # Return a list of (key, lsit) pairs ordered by depth
    return key_list_pairs


def match_word_substr(base_word, substr, word_set):
    if substr in word_set:
        return substr
    for pfx in PFX_SORTED:
        new_word = pfx + substr
        if new_word in word_set and new_word != base_word:
            return new_word
    for sfx in SFX_SORTED:
        new_word = substr + sfx
        if new_word in word_set and new_word != base_word:
            return new_word
    return None


def match_word_set(word, word_set):
    """Find a substring (using suffix list) of word in word_set"""
    word_match = match_word_substr(word, word, word_set)
    if word_match is not None:
        return word_match
    substr_left = word
    substr_right = word
    while len(substr_left) >= 0.5*len(word):
        substr_left = substr_left[:-1]
        word_match = match_word_substr(word, substr_left, word_set)
        if word_match is not None:
            return word_match
        substr_right = substr_right[1:]
        word_match = match_word_substr(word, substr_right, word_set)
        if word_match is not None:
            return word_match
    return None


def merge_hyp_dicts(hypo_dict, hyper_dict):
    """Merge child-to-parent relations in hyper into the hypo dict"""
    for c_key, p_keys in hyper_dict.items():
        if c_key not in hypo_dict:
            hypo_dict[c_key] = []
        for p_key in p_keys:
            if p_key not in hypo_dict:
                hypo_dict[p_key] = [c_key]
            elif c_key not in hypo_dict[p_key] and p_key != c_key:
                hypo_dict[p_key].append(c_key)
    for w_key in list(hypo_dict.keys()):
        for c_key in hypo_dict[w_key]:
            if c_key not in hypo_dict:
                hypo_dict[c_key] = []
    return hypo_dict


def simplify_dict(word_dict):
    """Remap any key or value which is not a single word in word_dict
    :param word_dict: a dictionary mapping word:word_list
    :return: word_dict"""
    # Normalize all keys in the dictionary
    for w_key in list(word_dict.keys()):
        # Split the key into component words
        key_list = wh.simplify_key(w_key)
        if len(key_list) > 1:
            # Add values from w_key to simplified dicationary key entry
            wh.dict_append(word_dict, key_list[0], word_dict[w_key])
            # For each word component, append to simple key entry
            wh.dict_append(word_dict, key_list[0], key_list[1:])
            # This key has been remapped; remove the complex version
            del word_dict[w_key]
    # Normalize all values in the dictionary
    dict_keys = list(word_dict.keys())
    for w_key in dict_keys:
        w_set = set()
        # Remove all complex word values from each dictionary value list
        for w in word_dict[w_key]:
            key_list = wh.simplify_key(w)
            w_set.add(key_list[0])
            if len(key_list) > 1:
                wh.dict_append(word_dict, key_list[0], key_list[1:])
        # Remove duplicate values from each entry
        word_dict[w_key] = [w for w in w_set if w != w_key]
    return word_dict


def trim_hierarchy(hypo_dict, key_list, root_key):
    """Return a hyponym dictionary containing only words in key_list"""
    key_set = set(key_list)
    assert root_key in hypo_dict and root_key in key_set
    # Remove values not found in key_set
    for k, v in hypo_dict.items():
        hypo_dict[k] = [w for w in v if w in key_set]
    # Extend relations to values of keys not found in key_set
    lost_keys = {k for k in hypo_dict.keys() if k not in key_set}
    key_queue = [(None, root_key)]
    parsed_set = set()
    while len(key_queue) > 0:
        p_key, w_key = key_queue.pop(0)
        w_list = hypo_dict[w_key]
        if w_key in lost_keys:
            hypo_dict[p_key].extend(w_list)
            w_key = p_key
        new_keys = [w for w in w_list if w not in parsed_set]
        for k in new_keys:
            parsed_set.add(k)
            key_queue.append((w_key, k))
    return {k:list(set(v)) for k, v in hypo_dict.items() if k in key_set}


def trim_list(word_dict, key_list):
    """Return a list of values and keys in word_dict shared with key_list"""
    word_list = []
    key_set = set(key_list)
    for k, v in word_dict.items():
        if k in key_set:
            word_list.append(k)
        word_list.extend([w for w in v if w in key_set])
    return list(set(word_list))