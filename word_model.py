"""
Word tree data persistence, access and management layer
tracks unique word key nodes and two-way references; initializes trees and
generates word vector encodings using the collection of tree objects
"""

from random import randint, choice
from tabulate import tabulate
import numpy as np
import word_helper as wh
import word_io as io
import word_logs as wl
import word_maps as wm
import word_progress as wp
import word_subsets as ws
import word_tagger as wt
import word_tree as tr


END_TOKEN = '<EOL>'  # end of line key
ENTITY_TOKEN = '<ENT>'  # named entity key
NULL_TOKEN = '<NUL>'  # null value key
NUMERIC_TOKEN = '<NUM>'  # numeric key
OOV_TOKEN = '<OOV>'  # out of vocab key
PUNCT_TOKEN = '<PNC>'  # punctuation key

MODEL_PATH = wh.SAVE_PATH + 'model\\'
LOG_FILE = wl.WordLogs(max_entries=1000)
LOG_PATH = wh.SAVE_PATH + 'logs\\'
INT_PATH = LOG_PATH + 'int_log.txt'
REC_PATH = LOG_PATH + 'rec_log.txt'


class WordModel(object):
    """Tagging, forest initialization and sample integration"""
    def __init__(self, stack_priors=2, word_tree=None):
        self.key_stack = WordStack(prior_cols=stack_priors)  # Working memory
        # Tag and token maps for initial path seeding
        self.root_map = wm.get_root_map()
        root_keys = set(self.root_map.values())
        self.tag_lookup, self.tag_map = wm.get_tag_map(root_keys)
        self.token_lookup, self.token_map = wm.get_token_map(root_keys)
        self.word_tagger = wt.WordTagger(wh.SAVE_PATH)
        self.word_tree = word_tree
        if self.word_tree is None:
            stack_layers = self.key_stack.key_layers
            self.word_tree = tr.WordTree(stack_layers)
        self._update_maps()

    def __contains__(self, word_key):
        """Return true if the key is found in tag_map"""
        return word_key in self.root_map

    def __len__(self):
        """Return the number of word keys contained in token_map"""
        return len(self.tag_map)

    def _update_maps(self):
        """Add token values to tag and token maps"""
        self.tag_map[END_TOKEN] = [-1]
        self.token_map[END_TOKEN] = [-1]
        self.tag_map[NUMERIC_TOKEN] = [0]
        self.token_map[NUMERIC_TOKEN] = [0]
        self.tag_map[OOV_TOKEN] = [0]
        self.token_map[OOV_TOKEN] = [0]

    @classmethod
    def from_file(cls, file_path):
        print("FROM_FILE: loading model...", end='', flush=True)
        tree_obj = wh.load_pickle(file_path)
        if tree_obj is None:
            print("\rFROM_FILE: model data at %s not found" % file_path)
            return None
        print("\rFROM_FILE: operation complete.")
        return cls(word_tree=tree_obj)

    def get_column(self, key_item):
        """Return the (pos_idx, tag_idx, token_idx) column from key_item"""
        k = self.get_key(key_item[1])  # key_item in the form (pos_idx, key_str)
        tag_idx, tkn_idx = choice(self.tag_map[k]), choice(self.token_map[k])
        return key_item[0], tag_idx, tkn_idx

    def get_inference(self, context_key, word_str, p_default=False):
        """Return the model inference following the prompt word_str"""
        # Assume word_str has not yet been tokenized
        word_list = self.tokenize_string(word_str)
        self.reset_stack(context_key, seed_item=(0, context_key))
        for w_item in word_list:
            self.key_stack.update_next(self.get_column(w_item))
        return self.word_tree.get_leaf(self.key_stack, p_default)

    def get_key(self, word_str):
        """Return the forest word key representation for word_str"""
        if word_str in self.root_map:
            return self.root_map[word_str]
        elif wh.match_numeric(word_str):
            return NUMERIC_TOKEN
        # word_str = wh.trim_unicode(word_str)
        word_str = wh.trim_numerics(word_str)
        word_str = word_str.lower()  # delay lower for TOKEN keys
        if word_str in self.root_map:
            return self.root_map[word_str]  # normalized representation match
        return OOV_TOKEN

    def integrate_samples(self, sample_dict):
        """Add all formatted dictionary samples into the forest graph"""
        for step, (context_key, sample_items) in enumerate(sample_dict.items()):
            self.reset_stack(context_key, seed_item=(0, context_key))
            for s_item in sample_items:  # in the form (pos_idx, key_str)
                next_col = self.get_column(s_item)
                self.key_stack.update_next(next_col, s_item[1])
                self.word_tree.map_state(self.key_stack)
            end_col = self.get_column((0, END_TOKEN))
            self.key_stack.update_next(end_col, END_TOKEN)
            self.word_tree.map_state(self.key_stack)
            wp.print_iterations(step, "INTEGRATE_DICT:")
        print("\rINTEGRATE_DICT: operation complete.")

    def print_tree(self):
        """Print tree and subnet dimensions"""
        print("PRINT_STATS: evaluating tree...", end='', flush=True)
        dim_list, headers = self.word_tree.get_tree_dimensions()
        node_count = sum([l[-1] for l in dim_list])
        out_str = "\rPRINT_STATS: {} vocab keys, {} key nodes, {} nodes\n"
        print(out_str.format(len(self), len(self.word_tree), node_count))
        print(tabulate(dim_list, headers=headers, showindex=True))
        print("\nPRINT_STATS: operation complete.")

    def prune_tree(self, min_value=0.01):
        """Remove all keys and edges from word_forest if ref_count < min_value"""
        self.word_tree.prune_tree(min_value)

    def reconstruct_samples(self, sample_dict, p_default=False):
        """Attempt to reconstruct each sample and update tree accordingly"""
        s_correct, s_count = 0, 0
        for step, (context_key, sample_items) in enumerate(sample_dict.items()):
            self.reset_stack(context_key, seed_item=(0, context_key))
            for s_item in sample_items:  # sample in the form (pos_idx, str)
                self.key_stack.target_key = s_item[1]
                s_correct += self.word_tree.map_leaf(self.key_stack, p_default)
                s_count += 1
            wp.print_iterations(step, "RECONSTRUCT_DICT:")
        print("\rRECONSTRUCT_DICT: %s/%s inferences." % (s_correct, s_count))

    def reset_stack(self, context_key, seed_item):
        """Reset all columns in key_stacks to the initial state"""
        self.key_stack.reset(context_key)
        seed_col = self.get_column(seed_item)
        self.key_stack.update_next(seed_col, seed_item[1])

    def to_file(self, file_path):
        """Save the forest graph context and key object files at save_path"""
        print("TO_FILE: saving model...", end='', flush=True)
        wh.save_pickle(file_path, self.word_tree)
        print("\rTO_FILE: operation complete.")

    def tokenize_string(self, raw_str):
        """Return the normalized list of token items from raw_str"""
        norm_str = wh.norm_string(raw_str)
        token_list = self.word_tagger.get_line_pos(norm_str)
        token_list = [(p, self.get_key(s)) for p, s in token_list]
        # Match word_str with forest vocab
        return token_list


class WordStack(object):
    """Dynamic contextual memory object in the form (F, P, N)"""
    def __init__(self, prior_cols=2, stack_layers=3):
        assert prior_cols >= 1  # Stack must include at least a single prior
        self.context_key, self.target_key = None, None
        self.prior_cols = prior_cols  # Account for F and N columns [priors +2]
        self.key_layers = stack_layers  # Key stack with pos, tag and tkn layers
        self.key_cols = self.prior_cols + 2  # Additional F and N columns
        stack_dim = (self.key_layers, self.key_cols)
        self.key_stack = np.full(stack_dim, 0, dtype=object)  # Initialize with 0

    def __getitem__(self, item_indices):
        """Return the stack key at item_indices"""
        return self.key_stack[item_indices[0], item_indices[1]]

    def __setitem__(self, item_indices, stack_key):
        """Set the stack key at item_indices"""
        self.key_stack[item_indices[0], item_indices[1]] = stack_key

    def get_layer_keys(self, layer_index, include_next=True):
        """Return tuple of Focus, Prior keys; exclude Next by default"""
        if include_next:
            return tuple(self.key_stack[layer_index])
        return tuple(self.key_stack[layer_index, :-1])

    def reset(self, context_key):
        """Re-initialize stack to contain only the columns in context"""
        self.context_key = context_key
        stack_dim = (self.key_layers, self.key_cols)
        self.key_stack = np.full(stack_dim, 0, dtype=object)

    def update_focus(self, focus_col):
        """Update the stack with the supplied focus key column"""
        # Shift F, P columns right
        self.key_stack[:, 1:-1] = self.key_stack[:, :-2]
        self.key_stack[:, 0] = focus_col

    def update_next(self, next_col, target_key=None):
        """Update the stack with the supplied focus key column"""
        self.update_focus(self.key_stack[:, -1])  # Shift current next to focus
        self.key_stack[:, -1] = next_col  # Update next column
        self.target_key = target_key


########################  WORD_MODEL DEBUG LOGIC  ########################


def get_debug_model(integrate_passes, overwrite=False):
    print("GET_DEBUG_MODEL: fetching model...")
    def_path, save_path = 'model\\def_dict.dat', MODEL_PATH + 'word_model.dat'
    word_model = WordModel.from_file(save_path)
    if word_model is None or overwrite:
        print("\rGET_DEBUG_MODEL: building new model...")
        word_model = WordModel()
    def_args = [ws.get_wnet_definitions, word_model]
    def_dict = io.load_or_build(get_data_dict, def_args, def_path)
    for _ in range(integrate_passes):
        word_model.integrate_samples(def_dict)
    if integrate_passes > 0:
        word_model.to_file(save_path)
    print("\rGET_DEBUG_MODEL: operation complete.")
    return word_model


def get_data_dict(dict_function, word_model):
    """Load or build a key: [(pos, raw_str, token_str),...] sample dict"""
    iter_count, sample_dict = 0, dict_function()
    sample_dict = {k: v for k, v in sample_dict.items() if len(v) > 0}
    data_dict = {}
    for k in sample_dict.keys():
        data_dict[k] = []
        for pos_tag, sample in sample_dict[k]:
            token_sample = word_model.tokenize_string(sample)
            data_dict[k].extend(token_sample)
        iter_count += 1
        wp.print_iterations(iter_count, "GET_DATA_DICT:")
    print("\rGET_SAMPLE_DICT: operation complete.")
    return data_dict


def print_model_phrase(context_key, word_model, max_words=32):
    """Output a phrase from the given context key"""
    phrase_keys = [context_key]
    while len(phrase_keys) < max_words:
        s = ' '.join(phrase_keys)  # Create current phrase for next inference
        l_key = word_model.get_inference(context_key, s)
        if l_key == END_TOKEN:
            break
        phrase_keys.append(l_key)
    print('<{}>: {}'.format(context_key, ' '.join(phrase_keys)))


if __name__ == '__main__':
    # Debug tree functionality
    print("\n-- Start debug --\n")
    # Test integration functionality
    prune_model, reconstruct = False, True
    word_model = get_debug_model(integrate_passes=0, overwrite=False)
    if prune_model:
        word_model.prune_tree(min_value=0.05)
        word_model.print_tree()
        word_model.to_file(MODEL_PATH + 'word_model.dat')
    if reconstruct:
        exp_args = [ws.get_wnet_examples, word_model]
        exp_path = 'model\\exp_dict.dat'
        exp_dict = io.load_or_build(get_data_dict, exp_args, exp_path)
        word_model.integrate_samples(exp_dict)
        word_model.print_tree()
        for _ in range(8):
            word_model.reconstruct_samples(exp_dict)
    word_model.print_tree()
    # Test embedded memory and probabilistic associations
    '''print('\nTEST_OUTPUT:')
    seed_keys = ['she', 'because', 'time', 'test', 'plain', 'and', 'dfjg']
    for k in seed_keys:
        for _ in range(3):  # Output 3 associative patterns for each key
            print_model_phrase(k, word_model, max_words=32)'''
    print("\n-- End debug --")
