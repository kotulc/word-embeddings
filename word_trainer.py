"""
Train and evaluate feed forward and recurrent Neural Network models

Some code adapted from pytorch tutorials:
https://github.com/pytorch/examples/blob/master/mnist/main.py
"""

from __future__ import print_function
import math
import numpy
import os
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
import word_dataset as wd
import word_helper as wh


HYPER_PARAM_PATH = wh.SAVE_PATH + 'params.json'


class AttentionSubnet(nn.Module):
    """Wrapper for Feed Forward module with pairwise attention mechanism"""
    def __init__(self, param_dict):
        super(AttentionSubnet, self).__init__()
        # Module input vector: [state_dim elements, attention_dim elements]
        hidden_dim = param_dict['input_dim'] * 2  # Paired offset samples
        self.hidden_linear = nn.Linear(hidden_dim, param_dict['input_dim'])
        self.output_layer = nn.Linear(param_dict['input_dim'], param_dict['output_dim'])
        #self.output_subnet = FeedForwardSubnet(param_dict)

    def forward(self, x):
        """Perform pairwise attention operations on the input sequence"""
        x_offset = torch.FloatTensor(x.size())
        x_offset[:, 1:] = x[:, :-1]  # Copy x tensor values with an offset
        x_offset[:, 0] = x[:, -1]
        # Create paired offset tensor by stacking offset and input tensors
        x = torch.cat((x, x_offset), 2)
        y = self.hidden_linear(x)  # Linear reduction to input_dim dimension
        y = torch.sum(y, 1)
        return self.output_subnet.forward(y)

    def report(self):
        """Output model parameter data"""
        hidden_sum = torch.sum(self.hidden_linear.weight.data)
        print("ATTN_SUBNET: hidden_linear sum {}".format(hidden_sum))
        self.output_subnet.report()


class FeedForwardSubnet(nn.Module):
    """Feed forward neural network with optional dropout, batch norm elements"""
    def __init__(self, param_dict):
        super(FeedForwardSubnet, self).__init__()
        self.batch_norm = param_dict['batch_norm']
        self.drop_out = param_dict['drop_out']
        self.hidden_layers = nn.ModuleList()
        input_dim = param_dict['input_dim']
        output_dim = param_dict['output_dim']
        self.residual_out = None
        if param_dict['residual_layers']:
            # The number of components before the residual connection
            self.residual_out = self.batch_norm + 2
        h_layers = param_dict['hidden_layers']
        layer_nodes = self.layer_nodes(h_layers, input_dim, output_dim)
        node_mult = param_dict['node_multiplier']
        layer_nodes = [math.ceil(v * node_mult) for v in layer_nodes]
        residual_dim = 0
        for l_nodes in layer_nodes:
            self.add_layer(input_dim + residual_dim, l_nodes)
            if self.residual_out is not None:
                input_dim, residual_dim = l_nodes, input_dim
            else:
                input_dim = l_nodes
        self.linear_out = nn.Linear(input_dim + residual_dim, output_dim)

    def forward(self, x):
        """Forward propagate x through all hidden layers"""
        x_residual = x
        layer_idx = 1
        for h_layer in self.hidden_layers:
            x = h_layer(x)
            if layer_idx == self.residual_out:
                x_hidden = x
                x = torch.cat([x, x_residual], 1)
                x_residual = x_hidden
                layer_idx = 0
                if self.drop_out:
                    layer_idx -= 1
            layer_idx += 1
        return self.linear_out(x)

    def add_layer(self, input_dim, layer_nodes):
        """Add a new hidden feed forward layer to this module"""
        self.hidden_layers.append(nn.Linear(input_dim, layer_nodes))
        self.hidden_layers.append(nn.ReLU6())
        if self.batch_norm:
            self.hidden_layers.append(nn.BatchNorm1d(layer_nodes))
        if self.drop_out:
            self.hidden_layers.append(nn.Dropout(p=0.5))

    def layer_nodes(self, hidden_layers, input_dim, output_dim):
        """Return a list of evenly spaced hidden layer node counts"""
        total_layers = hidden_layers + 2
        node_counts = numpy.linspace(input_dim, output_dim, total_layers)
        node_counts = node_counts[1:-1] # Trim the first and last entry
        return [int(n) for n in node_counts]

    def report(self):
        """Output model parameter data"""
        linear_sum = torch.sum(self.linear_out.weight.data)
        print("FF_SUBNET: linear_out sum {}".format(linear_sum))


class TestData(object):
    """Functionality to collect and evaluate model output"""
    def __init__(self, test_dataset, params):
        self.test_ds = test_dataset
        self.params = params
        test_samples = len(test_dataset) * params['test_rate']
        self.batch_count = math.ceil(test_samples / params['test_batch'])
        self.batch_size = params['test_batch']
        self.loss = 0
        self.test_samples = min(
            (len(test_dataset), int(self.batch_count * params['test_batch']))
        )
        self.output = torch.FloatTensor(self.test_samples, test_dataset.y_dim)
        self.out_idx = torch.zeros((self.test_samples, ))

    def __len__(self):
        return self.test_samples

    def average_loss(self):
        return self.loss / self.test_samples

    def accuracy(self):
        """Return the number of correct inferences for the given problem"""
        targets = self.get_targets()
        if self.params['problem_type'] == 'regression':
            criterion = nn.MSELoss(reduction='none')
            loss = criterion(self.output, targets)
            loss = torch.sum(loss, dim=1) / loss.size()[1]
            regr_target = loss < self.params['regr_target']
            correct = torch.sum(regr_target)
        else:
            # get the index of the max log-probability
            criterion = nn.CrossEntropyLoss(reduction='none')
            loss = criterion(self.output, targets)
            model_inf = self.output.max(1, keepdim=True)[1]
            correct = model_inf.eq(targets.view_as(model_inf)).sum().item()
        return int(correct), loss

    def correct(self):
        return self.accuracy()[0]

    def display_confusion(self, display_count=10):
        """Output the top display_count errors from word_model inferences"""
        correct, loss = self.accuracy()
        print()
        out_str = "TEST_DATA: key: {}, loss: {}, closest: {}, delta: {}"
        for _ in range(display_count):
            max_idx = int(torch.argmax(loss))  # Select greatest loss
            idx, max_loss = int(self.out_idx[max_idx]), loss[max_idx]
            output = self.output[max_idx]
            target_key = self.test_ds.get_keys(idx)
            '''input, _, _ = self.test_ds[idx]
            target_deltas = torch.abs(self.test_ds.y_tensor - output)
            min_idx = int(torch.argmin(target_deltas))
            closest_key = self.test_ds.get_key(min_idx)
            closest_sample, _, _ = self.test_ds[min_idx]
            input_delta = torch.sum(torch.abs(closest_sample - input))
            print(out_str.format(target_key, max_loss, closest_key, input_delta))'''
            loss[max_idx] = -1  # Bury this item for the next iteration

    def get_targets(self):
        """Return the tensor of sample targets relative to sample indices"""
        if self.params['problem_type'] == 'regression':
            targets = torch.FloatTensor(self.test_samples, self.test_ds.y_dim)
        else:
            targets = torch.LongTensor(self.test_samples)
        for idx in range(self.out_idx.size()[0]):
            _, target, _ = self.test_ds[int(self.out_idx[idx])]
            targets[idx] = target
        return targets

    def update(self, batch_idx, batch_loss, batch_out, batch_step):
        start_idx = batch_step * self.batch_size
        end_idx = min((start_idx + self.batch_size, self.test_samples))
        self.out_idx[start_idx:end_idx] = batch_idx
        self.output[start_idx:end_idx] = batch_out
        self.loss += batch_loss


class WordTrainer(object):
    """Initialize, train and manage generated Pytorch models"""
    def __init__(self, params, test_dataset, train_dataset):
        """Return a new generic model generated from train/test datasets"""
        self.model_params = params['model_params']
        self.train_params = params['training_params']
        use_cuda = not self.train_params['no_cuda'] and torch.cuda.is_available()
        torch.manual_seed(self.train_params['rand_seed'])
        self.device = torch.device("cuda" if use_cuda else "cpu")
        kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
        # Initialize related data and model objects
        self.train_loader = torch.utils.data.DataLoader(
            train_dataset, batch_size=self.train_params['training_batch'],
            shuffle=True, **kwargs
        )
        self.test_loader = torch.utils.data.DataLoader(
            test_dataset, batch_size=self.train_params['test_batch'],
            shuffle=False, **kwargs
        )
        if self.model_params['model_type'] == 'sequential':
            self.model = AttentionSubnet(self.model_params)
        else:
            self.model = FeedForwardSubnet(self.model_params)
        self.model = self.model.to(self.device)
        self.optimizer = optim.SGD(
            self.model.parameters(), lr=self.train_params['lr_min'],
            momentum=self.train_params['sgd_momentum']
        )
        # Determine loss function
        if self.train_params['problem_type'] == 'regression':
            self.criterion = nn.MSELoss(reduction='sum')
        else:
            self.criterion = nn.CrossEntropyLoss(reduction='sum')

    def build_model(self):
        print("BUILD_MODEL: ---- New model ---- ")
        # Output primary model parameters
        out_str = 'BUILD_MODEL: node_mult {}, h_layers {}, res_layers {}'
        out_str = out_str.format(
            self.model_params['node_multiplier'], self.model_params['hidden_layers'],
            self.model_params['residual_layers']
        )
        print(out_str)
        self.train_params['lr_max'] = self.get_lr_max()
        test_data, loss_list = self.train_model()  # Perform model training
        print("BUILD_MODEL: model min loss %f" % min(loss_list))

    @classmethod
    def from_file(cls, params, model_path, test_dataset, train_dataset):
        w_model = cls(params, test_dataset, train_dataset)
        w_model.model.load_state_dict(torch.load(model_path))
        return w_model

    def get_lr_max(self):
        """Estimate the maximum learning rate for this model"""
        self.model.train(True)  # Set to training mode
        batch_loss, early_stop, min_loss, step_count = [], False, None, 1
        lr_interval = self.train_params['lr_interval']
        lr_iter, lr_max = self.train_params['lr_min'], self.train_params['lr_min']
        out_str = "\rGET_LR_MAX: iteration %d, lr_max %f"
        while step_count < self.train_params['test_interval'] and not early_stop:
            for data, target, idx in self.train_loader:
                loss = self.train_batch(data, target)
                batch_loss.append(float(loss))
                if step_count % self.train_params['log_interval'] == 0:
                    print(out_str % (step_count, lr_max), end='', flush=True)
                if step_count % lr_interval == 0:
                    if average_gradient(batch_loss, lr_interval) > 0:
                        early_stop = True
                        break
                    lr_iter *= 2
                if min_loss is None or batch_loss[-1] < min_loss:
                    min_loss = batch_loss[-1]
                    lr_max += lr_iter
                    for p_group in self.optimizer.param_groups:
                        p_group['lr'] = lr_max  # Update optimizer learning rate
                step_count += 1
        # Reset all weights to their initial values
        self.model.apply(reset_weights)
        print("\rGET_LR_MAX: iteration %d, lr_max %f" % (step_count, lr_max))
        return lr_max

    def print_status(self, batch_loss, epoch, step, train_ds=True):
        if train_ds:
            out_str = 'TRAIN_MODEL: epoch {:<4} step {:<6} '
            avg_loss = batch_loss / self.train_params['training_batch']
            sample_len = len(self.train_loader.dataset)
            sample_n = (step * self.train_params['training_batch']) % sample_len
        else:
            out_str = 'TEST_MODEL: epoch {:<4} step {:<6} '
            avg_loss = batch_loss / self.train_params['test_batch']
            sample_len = len(self.test_loader.dataset)
            sample_n = (step * self.train_params['test_batch']) % sample_len
        out_str += '\t{}/{}, \tavg/batch loss {:^10.6} {:^10.6}'
        out_str = out_str.format(
            epoch, step, sample_n, sample_len, avg_loss, batch_loss
        )
        print('\r' + out_str, end='', flush=True)

    def save_model(self, save_path):
        """Create the directory if it does not yet exist and save model"""
        os.makedirs(os.path.dirname(save_path), exist_ok=True)
        torch.save(self.model.state_dict(), save_path)

    def test_model(self):
        """Evaluate the models performance against the test dataset"""
        print("\rTEST_MODEL: evaluating model...", end='', flush=True)
        self.model.eval()  # Set to test/evaluation model
        test_data = TestData(self.test_loader.dataset, self.train_params)
        with torch.no_grad():
            for step, (data, target, idx) in enumerate(self.test_loader):
                data, target = data.to(self.device), target.to(self.device)
                output = self.model(data)
                batch_loss = float(self.criterion(output, target))
                test_data.update(idx, batch_loss, output, step)
                self.print_status(batch_loss, 0, step, train_ds=False)
                if step >= test_data.batch_count:
                    break  # Quit after testing test_rate fraction of data
        out_str = "\rTEST_MODEL: accuracy {}/{}, loss {:^10.6}"
        avg_loss = test_data.average_loss()
        print(out_str.format(test_data.correct(), len(test_data), avg_loss))
        self.model.report()
        return test_data

    def train_batch(self, data, target):
        """Perform a training iteration with the batch data"""
        data, target = data.to(self.device), target.to(self.device)
        output = self.model(data)
        loss = self.criterion(output, target)
        self.optimizer.zero_grad()
        loss.backward()
        # Prevent the exploding gradient problem
        grad_clip = self.train_params['grad_clip']
        torch.nn.utils.clip_grad_norm_(self.model.parameters(), grad_clip)
        self.optimizer.step()
        return float(loss.data)

    def train_model(self):
        """Run epoch training iterations until convergence or no progress is made"""
        print("TRAIN_MODEL: running training...")
        test_data = self.test_model()
        self.model.train(True)  # Set to training mode
        loss_list, lr_list = [], []
        epoch, step = 1, 0
        # step size is the number of iterations in half a cycle
        while step < self.train_params['max_steps']:
            for batch_idx, (data, target, idx) in enumerate(self.train_loader):
                batch_loss = self.train_batch(data, target)
                sample_iter = batch_idx * self.train_params['training_batch']
                lr_val = self.update_lr(sample_iter)
                step += 1
                if step % self.train_params['log_interval'] == 0:
                    lr_list.append(lr_val)
                    self.print_status(batch_loss, epoch, step)
                    loss_list.append(batch_loss)
                if step % self.train_params['test_interval'] == 0:
                    # Asses model on test dataset and calculate correct inferences
                    test_data = self.test_model()
            # Reduce the difference in min and max learning rate after each epoch
            lr_delta = (self.train_params['lr_max'] - self.train_params['lr_min'])
            lr_delta *= self.train_params['lr_decay']
            self.train_params['lr_max'] -= lr_delta
            self.train_loader.dataset.refresh()
            epoch += 1
        if self.train_params['plot_data']:
            step_str = 'step (x{})'.format(self.train_params['log_interval'])
            plot_data(loss_list, 'Batch loss', step_str, 'loss')
            plot_data(lr_list, 'Batch lr', step_str, 'lr')
            test_data.display_confusion()
        print("TRAIN_MODEL: training complete.")
        return test_data, loss_list

    def update_lr(self, sample_itr):
        """Update the optimizer learning rate parameters"""
        # Cyclical learning rate update used for model training
        ds_size = len(self.train_loader.dataset)
        step_size = 2 * (ds_size / self.train_params['training_batch'])
        lr_max, lr_min = self.train_params['lr_max'], self.train_params['lr_min']
        lr_cycle = math.floor(1 + sample_itr / (2 * step_size))
        x = abs(sample_itr / step_size - 2 * lr_cycle + 1)
        lr_val = lr_min + (lr_max - lr_min) * max(0, (1 - x))
        for p_group in self.optimizer.param_groups:
            p_group['lr'] = lr_val
        return lr_val


def average_gradient(data_list, window_len):
    """Return the avg. gradient of data_list points over window_len entries"""
    assert window_len >= 2
    assert len(data_list) >= window_len
    window_data = data_list[-window_len:]
    left_avg = numpy.average(window_data[:int(window_len*0.5)])
    right_avg = numpy.average(window_data[int(window_len*0.5):])
    return right_avg - left_avg


def get_params(save_path, overwrite=False, sequential=False, regression=False):
    """Attempt to load the file at save_path, else return the default params"""
    hyper_params = wh.load_json(save_path)
    if hyper_params is None or overwrite:
        hyper_params = {
            'training_params':{
                'grad_clip':5, 'log_interval':100, 'lr_decay':0.01,
                'lr_interval':250, 'lr_max':1e-5, 'lr_min':1e-5,
                'max_steps':128000, 'no_cuda':True, 'plot_data':True,
                'print_status':True, 'problem_type':'classification',
                'rand_seed':1, 'regr_target':0.10, 'sgd_momentum':0.5,
                'test_batch':1000, 'test_interval':8000, 'test_rate':1.0,
                'training_batch':64
            },
            'model_params':{
                'batch_norm':True, 'drop_out':True, 'hidden_layers':2,
                'model_type':'linear', 'node_multiplier':1.0,
                'residual_layers':True
            }
        }
        if regression:
            hyper_params['training_params']['problem_type'] = 'regression'
        if sequential:
            hyper_params['model_params']['model_type'] = 'sequential'
        wh.save_json(save_path, hyper_params)
    return hyper_params


def get_test_model(problem_type='regression'):
    """Return a randomly initialized regression model for testing"""
    regression = problem_type == 'regression'
    test_dataset = wd.RandDebugDataset(
        sample_n=100, x_dim=100, y_dim=5, regression=regression
    )
    return WordTrainer(problem_type, test_dataset, test_dataset)


def plot_data(data_list, title, x_label, y_label):
    """Plot the data values contained in data_list"""
    fig, ax = plt.subplots()
    ax.plot([i for i in range(0, len(data_list))], data_list)
    ax.set(xlabel=x_label, ylabel=y_label,
           title=title)
    ax.grid()
    plt.show()


def reset_weights(m):
    """Reset all model parameters"""
    if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
        m.reset_parameters()

