"""
Input and output related helper functions
"""

import gzip
import json
import numpy
import os.path
import pickle


# helper data path constants
DIR_PATH = os.path.dirname(os.path.realpath(__file__))
SAVE_PATH = DIR_PATH + '\\data\\'


def load_or_build(build_fcn, build_args, local_path, report=True):
    """Return file from file_path if found, else build with build_fcn"""
    file_path = SAVE_PATH + local_path
    if report:
        print(build_fcn.__name__.upper() + ': loading...', end='', flush=True)
    if file_path.endswith('.json'):
        file_obj = load_json(file_path)
        if file_obj is None:
            file_obj = build_fcn(*build_args)
            save_json(file_path, file_obj)
    else:  # Assume pickle file has been compressed
        file_obj = load_pickle(file_path, True)
        if file_obj is None:
            file_obj = build_fcn(*build_args)
            save_pickle(file_path, file_obj, True)
    if report:
        print('\r' + build_fcn.__name__.upper() + ': operation complete.')
    return file_obj


def load_json(file_str):
    if os.path.isfile(file_str):
        with open(file_str, 'r') as file_obj:
            json_obj = json.load(file_obj)
        return json_obj
    return None


def load_pickle(file_str, compressed=True):
    if os.path.isfile(file_str):
        if compressed:
            with gzip.open(file_str, 'rb') as file_obj:
                pkl_obj = pickle.load(file_obj)
        else:
            with open(file_str, 'rb') as file_obj:
                pkl_obj = pickle.load(file_obj)
        return pkl_obj
    return None


def load_text(file_str):
    if os.path.isfile(file_str):
        with open(file_str, 'r') as file_obj:
            lines = file_obj.readlines()
        return lines
    return None


def load_vectors(vect_path):
    if os.path.isfile(vect_path):
        vect_dict = {}
        with open(vect_path, 'r', encoding="utf8") as file_obj:
            for line in file_obj:
                split_line = line.split()
                vect_key = split_line[0]
                vect_value = numpy.array([float(val) for val in split_line[1:]])
                vect_dict[vect_key] = vect_value
        return vect_dict
    else:
        raise FileNotFoundError


def save_json(file_str, data_obj):
    os.makedirs(os.path.dirname(file_str), exist_ok=True)
    with open(file_str, 'w') as file_obj:
        json.dump(data_obj, file_obj)


def save_pickle(file_str, data_obj, compressed=True):
    os.makedirs(os.path.dirname(file_str), exist_ok=True)
    if compressed:
        with gzip.open(file_str, 'wb') as file_obj:
            pickle.dump(data_obj, file_obj, protocol=-1)
    else:
        with open(file_str, 'wb') as file_obj:
            pickle.dump(data_obj, file_obj, protocol=-1)