"""
Word processing and normalization helper functions
"""

import gzip
import json
import numpy
import os.path
import pickle
import re


# helper data path constants
DIR_PATH = os.path.dirname(os.path.realpath(__file__))
SAVE_PATH = DIR_PATH + '\\data\\'


def chunk_list(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def dict_add(dict_obj, key, val):
    """Add a new set element to dict_obj or add to an existing set"""
    if not isinstance(val, set): val = {val}
    if key in dict_obj: dict_obj[key] = dict_obj[key].union(val)
    else: dict_obj[key] = val
    return dict_obj


def dict_append(dict_obj, key, val):
    """Add a new list element to dict_obj or append to an existing list"""
    if not isinstance(val, list):
        val = [val]
    if key in dict_obj:
        dict_obj[key] += val
    else:
        dict_obj[key] = val
    return dict_obj


def dict_diff(key_list, dict_obj):
    """Return all word_list values that are not keys in word_dict"""
    return [w for w in key_list if w not in dict_obj]


def dict_merge(dest_dict, ext_dict, set_vals=False):
    """Merge two dictionaries; if a duplicate key is found append unique values"""
    for k, v in ext_dict.items():
        if set_vals:
            dict_add(dest_dict, k, v,)
        else:
            dict_append(dest_dict, k, v)
    return dest_dict


def extract_phrases(raw_str):
    return re.findall(r'\w+(?:[-_]\w+)+', raw_str)


def load_or_build(file_str, build_fcn, build_args, compressed=True):
    """Return file from file_str if found, else build with build_fcn"""
    if file_str.endswith('.json'):
        file_obj = load_json(file_str)
        if file_obj is None:
            file_obj = build_fcn(*build_args)
            save_json(file_str, file_obj)
    else:
        file_obj = load_pickle(file_str, compressed)
        if file_obj is None:
            file_obj = build_fcn(*build_args)
            save_pickle(file_str, file_obj, compressed)
    return file_obj


def load_json(file_str):
    if os.path.isfile(file_str):
        with open(file_str, 'r') as file_obj:
            json_obj = json.load(file_obj)
        return json_obj
    return None


def load_pickle(file_str, compressed=True):
    if os.path.isfile(file_str):
        if compressed:
            with gzip.open(file_str, 'rb') as file_obj:
                pkl_obj = pickle.load(file_obj)
        else:
            with open(file_str, 'rb') as file_obj:
                pkl_obj = pickle.load(file_obj)
        return pkl_obj
    return None


def load_text(file_str):
    if os.path.isfile(file_str):
        with open(file_str, 'r') as file_obj:
            lines = file_obj.readlines()
        return lines
    return None


def load_vectors(vect_path):
    if os.path.isfile(vect_path):
        vect_dict = {}
        with open(vect_path, 'r', encoding="utf8") as file_obj:
            for line in file_obj:
                split_line = line.split()
                vect_key = split_line[0]
                vect_value = numpy.array([float(val) for val in split_line[1:]])
                vect_dict[vect_key] = vect_value
        return vect_dict
    else:
        raise FileNotFoundError


def match_numeric(word_str):
    """Match strings that may be parsed as numeric values"""
    if word_str == 'infinity':
        return False
    try:  # Try to convert word_str into an integer
        float(word_str)
        return True
    except ValueError:
        return False


def match_prefix(word, prefix_list):
    for p in prefix_list:
        if word.startswith(p): return p
    return None


def match_punct(word_str):
    """Return true if any punctuation is present"""
    if re.match(r'[^\w\s]', word_str):
        return True
    return False


def match_special(word_str):
    # Return true if word is a special character
    if re.match(r'^[^\w\s]$', word_str):
        return True
    return False


def match_suffix(word, suffix_list):
    for s in suffix_list:
        if word.endswith(s): return s
    return None


def norm_key(key_str):
    # Match and remove all special characters
    key_str = re.sub(r'[^\w\s-]+', '', key_str.lower())
    return split_key(key_str)


def norm_string(raw_str):
    norm_str = raw_str.lower()
    # Match and remove all special characters
    norm_str = re.sub(r'[^\w\s-]+', '', norm_str)
    # Replace dashes or underscores with spaces
    norm_str = re.sub(r'[_-]+', ' ', norm_str)
    return norm_str


def partition_word(word, prefix_list, suffix_list):
    p = match_prefix(word, prefix_list)
    s = match_suffix(word, suffix_list)
    if len(s) == 0: return (word[len(p):], p, s)
    return (word[len(p):-len(s)], p, s)


def save_json(file_str, data_obj):
    os.makedirs(os.path.dirname(file_str), exist_ok=True)
    with open(file_str, 'w') as file_obj:
        json.dump(data_obj, file_obj)


def save_pickle(file_str, data_obj, compressed=True):
    os.makedirs(os.path.dirname(file_str), exist_ok=True)
    if compressed:
        with gzip.open(file_str, 'wb') as file_obj:
            pickle.dump(data_obj, file_obj, protocol=-1)
    else:
        with open(file_str, 'wb') as file_obj:
            pickle.dump(data_obj, file_obj, protocol=-1)


def simple_set(str_list):
    return set([w for s in str_list for w in str_to_set(s)])


def simplify_key(key_str):
    """Split the key into component words and return list"""
    key_list = norm_key(key_str)
    return [k for k in key_list if k != '']


def split_key(key_str):
    return re.split(r'[-_\s]', key_str)


def str_to_set(str_obj):
    """Return a set of word tokens"""
    if not isinstance(str_obj, list): return set(tokenize_string(str_obj))
    return set(tokenize_string(' '.join(str_obj)))


def strip_pos(word_str):
    re_result = re.search(r'^([\w-]+)\.', word_str)
    if re_result is None:
        return word_str
    return re_result.group(1)


def tokenize_norm(raw_str):
    return tokenize_string(norm_string(raw_str))


def tokenize_full(raw_str):
    norm_str = re.sub(r'[_-]+', ' ', raw_str)
    return re.findall(r"\w+|[^\w\s]", norm_str.lower(), re.UNICODE)


def tokenize_string(raw_str):
    return re.split(r'[\W+_]', raw_str.lower())


def trim_numerics(raw_str):
    return ''.join([i for i in raw_str if not i.isdigit()])


def trim_unicode(raw_str):
    return ''.join(i for i in raw_str if ord(i)<128)


def valid_set(val_list, dict_obj):
    """Return a set object containing only keys found in dict_obj"""
    return set([k for k in val_list if k in dict_obj])


def word_pos(word_str):
    re_result = re.search(r'\.([\w-]+)$', word_str)
    if re_result is None:
        return ''
    return re_result.group(1)