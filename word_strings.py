"""
String processing and normalization helper functions
"""

import re


def extract_phrases(raw_str):
    return re.findall(r'\w+(?:[-_]\w+)+', raw_str)


def match_numeric(word_str):
    """Match strings that may be parsed as numeric values"""
    if word_str == 'infinity':
        return False
    try:  # Try to convert word_str into an integer
        float(word_str)
        return True
    except ValueError:
        return False


def match_prefix(word, prefix_list):
    for p in prefix_list:
        if word.startswith(p): return p
    return None


def match_punctuation(word_str):
    """Return true if any punctuation is present"""
    if re.match(r'[^\w\s]', word_str):
        return True
    return False


def match_special(word_str):
    # Return true if word is a special character
    if re.match(r'^[^\w\s]$', word_str):
        return True
    return False


def match_suffix(word, suffix_list):
    for s in suffix_list:
        if word.endswith(s): return s
    return None


def norm_key(key_str):
    # Match and remove all special characters
    key_str = re.sub(r'[^\w\s-]+', '', key_str.lower())
    return split_key(key_str)


def norm_string(raw_str):
    norm_str = raw_str.lower()
    # Match and remove all special characters
    norm_str = re.sub(r'[^\w\s-]+', '', norm_str)
    # Replace dashes or underscores with spaces
    norm_str = re.sub(r'[_-]+', ' ', norm_str)
    return norm_str


def partition_word(word, prefix_list, suffix_list):
    p = match_prefix(word, prefix_list)
    s = match_suffix(word, suffix_list)
    if len(s) == 0: return (word[len(p):], p, s)
    return (word[len(p):-len(s)], p, s)


def simplify_key(key_str):
    """Split the key into component words and return list"""
    key_list = norm_key(key_str)
    return [k for k in key_list if k != '']


def split_key(key_str):
    return re.split(r'[-_\s]', key_str)


def str_to_set(str_obj):
    """Return a set of word tokens"""
    if not isinstance(str_obj, list): return set(tokenize_string(str_obj))
    return set(tokenize_string(' '.join(str_obj)))


def strip_pos(word_str):
    re_result = re.search(r'^([\w-]+)\.', word_str)
    if re_result is None:
        return word_str
    return re_result.group(1)


def tokenize_norm(raw_str):
    return tokenize_string(norm_string(raw_str))


def tokenize_full(raw_str):
    norm_str = re.sub(r'[_-]+', ' ', raw_str)
    return re.findall(r"\w+|[^\w\s]", norm_str.lower(), re.UNICODE)


def tokenize_string(raw_str):
    return re.split(r'[\W+_]', raw_str.lower())


def trim_numerics(raw_str):
    return ''.join([i for i in raw_str if not i.isdigit()])


def trim_unicode(raw_str):
    return ''.join(i for i in raw_str if ord(i) < 128)


def valid_set(val_list, dict_obj):
    """Return a set object containing only keys found in dict_obj"""
    return set([k for k in val_list if k in dict_obj])


def word_pos(word_str):
    re_result = re.search(r'\.([\w-]+)$', word_str)
    if re_result is None:
        return ''
    return re_result.group(1)
