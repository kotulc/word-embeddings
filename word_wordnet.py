"""
WordNet interface layer providing access to the raw word relations found
within this module. Used primarily by word_subsets.py
"""

from nltk.corpus import wordnet as wn
import word_progress as wp
import word_helper as wh


# Part of speech and relation constants
ATTR_KEYS = ['hyper', 'hypo', 'ant', 'ent', 'pert', 'derv']
ADJ, ADJ_SAT, ADV, NOUN, VERB = 'a', 's', 'r', 'n', 'v'
POS_KEYS = [ADJ, ADJ_SAT, ADV, NOUN, VERB]
REL_TYPES = ['hypr','hypo','synm','antm','pert','derv']


def add_attribute(dict_key, dict_obj, dict_value, set_idx=0):
    """Add an attribute value at set_idx for dict_key"""
    if dict_value is not None:
        dict_key = dict_key.lower()
        if dict_key not in dict_obj:
            dict_obj[dict_key] = (set(), set())  # (pos_set, attr_set)
        dict_obj[dict_key][set_idx].add(dict_value)


def get_attribute(attr_key, attr_list):
    """Return the attribute_key if attr_list contains any entries"""
    if len(attr_list) > 0:
        return attr_key
    return None


def wordnet_attributes(word_list):
    attr_dict, iter_count = {}, 0
    for w in word_list:
        attr_dict[w] = (set(), set())
        iter_count += 1
        for s in wn.synsets(w):
            for l in s.lemmas():
                attr_vals = [s.hypernyms(), s.hyponyms(), l.antonyms(),
                             l.entailments(), l.pertainyms(),
                             l.derivationally_related_forms()]
                add_attribute(w, attr_dict, s.pos(), 0)
                for a_idx in range(len(ATTR_KEYS)):
                    attr = get_attribute(ATTR_KEYS[a_idx], attr_vals[a_idx])
                    add_attribute(w, attr_dict, attr, 1)
        wp.print_iterations(iter_count, message='WORDNET_ATTRIBUTES:')
    return attr_dict


def wordnet_definitions(word_list=None, simple_dict=False):
    """
    For each word, get all related synset definition strings
    :param word_list: list of words to extract from wordnet
    :return def_dict: dictionary of word definitions
    """
    if word_list is None:
        word_list = wordnet_words()
    if simple_dict:
        def_dict = {k: '' for k in word_list}
    else:
        def_dict = {k: [] for k in word_list}
    iter_count = 0
    for w in word_list:
        synsets = wn.synsets(w)
        for s in synsets:
            if simple_dict:
                if len(def_dict[w]) > 0:
                    def_dict[w] += '; '
                def_dict[w] += s.definition()
            else:
                def_dict[w].append((s.pos(), s.definition()))
        iter_count += 1
        wp.print_iterations(iter_count, "WORDNET_DEFINITIONS:")
    return def_dict


def wordnet_examples(word_list):
    """
    For each word, get all related synset example strings
    :param word_list: list of words to extract from wordnet
    :return sentence_list: list of extracted sentence strings
    """
    exp_dict = {k:[] for k in word_list}
    iter_count = 0
    for w in word_list:
        synsets = wn.synsets(w)
        for s in synsets:
            if len(s.examples()) > 0:
                for exp in s.examples():
                    exp_dict[w].append([s.pos(), exp])
        iter_count += 1
        wp.print_iterations(iter_count, "WORDNET_EXAMPLES:")
    return exp_dict


def wordnet_words():
    """Extract all unique words from Wordnet"""
    out_str = "WORDNET_WORDS:"
    iter_count = 0
    word_set = set()
    for s in wn.all_synsets():
        iter_count += 1
        lemmas = s.lemmas()
        for lemma in lemmas:
            word_set.add(lemma.name())
        wp.print_iterations(iter_count, message=out_str)
    print("\rWORDNET_WORDS: operation complete.")
    return list(word_set)


########################  WORD_MAPS DEPRECATED LOGIC  ########################


def add_or_update(dict_key, dict_obj, dict_value, add_empty=False):
    """Add or update a list of unique strings to the dictionary"""
    dict_key = dict_key.lower()
    if not isinstance(dict_value, list):
        dict_value = [dict_value]
    val_list = [v.lower() for v in dict_value if v.lower() != dict_key]
    if len(val_list) == 0 and not add_empty: return
    if dict_key in dict_obj:
            dict_obj[dict_key] = list(set(dict_obj[dict_key] + val_list))
    else:
            dict_obj[dict_key] = list(set(val_list))


def lemma_extract(dict_key, dict_obj, lemmas):
    """Add a word lemma to dict_obj"""
    if len(lemmas) > 0:
        lemma_list = [l.name() for l in lemmas if l.name()]
        add_or_update(dict_key, dict_obj, lemma_list)


def lemma_keys(lemmas):
    """Return a list of lemma object names"""
    if len(lemmas) > 0:
        return [l.name() for l in lemmas]
    return []


def synset_hyp_extract(synset, word_dict, rel_type='hypr'):
    """Add hyponym or hypernym relation words to dict_obj"""
    word_list = [synset.name().split('.')[0]]
    if rel_type == 'hypr':
        hyp_list = [h.name().split('.')[0] for h in synset.hypernyms()]
    else:
        hyp_list = [h.name().split('.')[0] for h in synset.hyponyms()]
    lemmas = synset.lemmas()
    if len(lemmas) > 0:
        word_list.extend([l.name() for l in lemmas if l.name() != word_list[0]])
    for w in word_list:
        add_or_update(w, word_dict, hyp_list, add_empty=True)


def synset_rel_extract(synset, word_dict, add_empty=True, rel_type='hypr'):
    """Add rel_type relation word (from lemmas) to dict_obj"""
    if rel_type == 'hypr' or rel_type == 'hypo':
        synset_hyp_extract(synset, word_dict, rel_type)
    else:
        w = synset.name().split('.')[0]
        add_or_update(w, word_dict, [], add_empty=add_empty)
        lemmas = synset.lemmas()
        for lemma in lemmas:
            if rel_type == 'synm':
                add_or_update(w, word_dict, lemma.name())
            elif rel_type == 'antm':
                lemma_extract(w, word_dict, lemma.antonyms())
            elif rel_type == 'pert':
                lemma_extract(w, word_dict, lemma.pertainyms())
            else:
                lemma_extract(w, word_dict, lemma.derivationally_related_forms())


def wordnet_expand(word_list, word_dict=None, rel_type='synm'):
    """
    For each word, get synonyms, antonyms, direct antonyms, participial adjective,
    pertainym, postnominal meronym, holonym, derv. related forms and definitions
    :param word_list: list of words to extract from wordnet
    :return word_data: dictionary of dictionaries for each relation type
    """
    if word_dict is None:
        word_dict = {}
    iter_count = 0
    print("WORDNET_EXPAND: collecting data...")
    for w in word_list:
        synsets = wn.synsets(w)
        for s in synsets:
            synset_rel_extract(s, word_dict, add_empty=False, rel_type=rel_type)
        iter_count += 1
        pp.print_progress(iter_count, len(word_list))
    print("\rWORDNET_EXPAND: operation complete.")
    return word_dict


def wordnet_extract(word_pos, word_dict=None, rel_type='synm'):
    """Extract all nouns while retaining wordnet hierarchical structure"""
    out_str = "WORDNET_EXTRACT: iteration ="
    if word_dict is None:
        word_dict = {}
    iter_count = 0
    for s in wn.all_synsets(pos=word_pos):
        iter_count += 1
        synset_rel_extract(s, word_dict, rel_type=rel_type)
        wp.print_iterations(iter_count, message=out_str)
    print("\rWORDNET_EXTRACT: operation complete.")
    return word_dict


def wordnet_pos_list(word_key):
    rel_set = set()
    synsets = wn.synsets(word_key)
    for s in synsets:
        synset_pos = s.pos()
        rel_set.add(synset_pos)
    return list(rel_set)


def wordnet_relation_list(word_key, rel_type='all'):
    """Return a pos: [word, ...] dict of words related to word_key"""
    rel_list = []
    synsets = wn.synsets(word_key)
    for s in synsets:
        if rel_type == 'hyper' or rel_type == 'all':
            hyper_list = [h.name().split('.')[0] for h in s.hypernyms()]
            rel_list.extend(hyper_list)
        if rel_type != 'hyper':
            lemmas = s.lemmas()
            for lemma in lemmas:
                if rel_type == 'all' or rel_type == 'synm':
                    rel_list.append(lemma.name())
                if rel_type == 'all' or rel_type == 'antm':
                    rel_list.extend(lemma_keys(lemma.antonyms()))
                if rel_type == 'all' or rel_type == 'derv':
                    derv_forms = lemma.derivationally_related_forms()
                    rel_list.extend(lemma_keys(derv_forms))
                #if rel_type == 'all' or rel_type == 'pert':
                #    rel_dict[syn_pos].extend(lemma_keys(lemma.pertainyms()))
    return rel_list

