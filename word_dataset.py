"""
Word training datasets and word vectors for use in word_model.py
"""

from torch.utils.data import Dataset
import numpy as np
import os
import torch
import word_helper as wh
import word_subsets as ws


NULL_KEY = '<NUL>'
OOV_KEY = '<OOV>'

VECTS_PATH = wh.SAVE_PATH + 'vects\\'


class WordDataset(Dataset):
    """Template Dataset object for Word project"""
    def __init__(self, key_val_list, x_dim, y_dim, classification=True):
        self.key_val_list = key_val_list
        self.x_dim = x_dim
        self.y_dim = y_dim
        # x_tensor contains input samples and y_tensor are output targets
        self.x_tensor = torch.FloatTensor(len(self), self.x_dim)
        if classification:
            self.y_tensor = torch.LongTensor(len(self))
        else:
            self.y_tensor = torch.FloatTensor(len(self), self.y_dim)

    def __getitem__(self, idx):
        return self.x_tensor[idx], self.y_tensor[idx], idx

    def __len__(self):
        return len(self.key_val_list)

    def get_key(self, idx):
        return self.key_val_list[idx][0]

    def refresh(self):
        print("\rDS_REFRESH: updating samples... ", end='', flush=True)
        # Epoch data refresh logic goes here
        print("\rDS_REFRESH: operation complete.\r ", end='', flush=True)


class WordVectors(object):
    """"""
    def __init__(self, key_list, vect_path='glove.6B.50d.txt'):
        self.dict_path = VECTS_PATH + 'vect_dict.dat'
        self.vect_dict, self.vect_len = self.load_vectors(key_list, vect_path)
        self.vect_dict[NULL_KEY] = np.zeros(self.vect_len)
        self.vect_dict[OOV_KEY] = np.ones(self.vect_len)

    def __getitem__(self, w_key):
        return self.vect_dict[w_key]

    def __len__(self):
        return len(self.vect_dict)

    def load_vectors(self, key_list, vect_path):
        """Load and return the vector dictionary for key_list and vector len"""
        print("LOAD_VECTORS: loading vectors...", end='', flush=True)
        vect_dict = wh.load_pickle(self.dict_path)
        if vect_dict is None:  # No cached vector dictionary file
            if os.path.exists(vect_path):
                vect_dict = wh.load_vectors(vect_path)
                wh.save_pickle(self.dict_path, vect_dict)
            else:  # Create the vects directory if it does not already exist
                os.makedirs(os.path.dirname(vect_path), exist_ok=True)
                error_str = "\nLOAD_VECTORS: file {} does not exist."
                raise FileNotFoundError(error_str.format(vect_path))
        vect_dict = {k: vect_dict[k] for k in key_list if k in vect_dict}
        k_delta = abs(len(key_list) - len(vect_dict))
        if k_delta > 0:
            raise IOError("\nLOAD_VECTORS: {} missing keys".format(k_delta))
        print("\rLOAD_VECTORS: operation complete.")
        return vect_dict, len(next(iter(vect_dict.values())))


class WordContext(Dataset):
    """Word vector samples that map tree activations to node output words"""
    def __init__(self, context, model, samples, vectors, window_len=8):
        self.window_len = window_len
        self.sample_list = []  # list of context key tuples
        self.target_list = []  # list of target branch indices
        self.vect_len, self.word_vectors = vectors.vect_len, vectors
        self.word_model = model
        self.x_dim = self.vect_len * (window_len * 2 + 1)
        self.y_dim = model.relation_branches
        # Build the sample list (x) in the form (C.p, C.s0, ..., K.0, ...)
        for line_idx in range(len(samples) - 1):
            context_line = []  # Secondary relation context keys
            seed_line, target_line = samples[line_idx:line_idx + 1]
            self.word_model.set_context()
            for t_key, t_idx in enumerate(target_line):
                context_keys = self.get_window(context_line, t_idx)
                seed_keys = self.get_window(seed_line, t_idx)
                self.sample_list.append((context + context_keys + seed_keys))
                reset_stack = not t_idx  # Reset on the first iteration
                y_idx, y_key = model.branch_query(context, t_key, reset_stack)
                self.target_list.append(y_idx)  # Add target relation branch index
                context_line.append(y_key)  # Add relation key to secondary context
            context = self.word_model.context_query(context_line)

    def __getitem__(self, idx):
        sample_keys = self.sample_list[idx]
        x = torch.FloatTensor(self.x_dim)
        for w_key, k_idx in enumerate(sample_keys):
            start_idx = self.vect_len * k_idx
            x[start_idx:start_idx + self.vect_len] = self.word_model[w_key]
        return x, torch.LongTensor(self.target_list[idx])

    def __len__(self):
        return len(self.sample_list)

    def get_window(self, key_list, offset_idx):
        """Select window_len key slice from key_list, offset by offset_idx"""
        return None


########################  DEPRECATED LOGIC  ########################


class NodeDecodeDataset(Dataset):
    """Word vector samples that map tree activations to node output words"""
    def __init__(self, word_vectors, sample_n=8000, noise_stddev=0):
        self.y_dim, self.x_dim = word_vectors.shape
        # Scale numpy matrix to sample_n rows
        if sample_n > word_vectors.shape[0]:
            tile_n = sample_n // word_vectors.shape[0]
            word_vectors = np.tile(word_vectors, (tile_n, 1))
        # Add noise to each sample
        if noise_stddev > 0:
            word_rows, word_cols = word_vectors.shape
            noise_mtx = np.random.normal(size=(word_rows, word_cols))
            # Add random scale to each sample tile?
            word_vectors = word_vectors + (noise_mtx * noise_stddev)
        # Convert all numpy arrays to torch tensor objects
        self.x_samples = torch.from_numpy(word_vectors).float()

    def __getitem__(self, idx):
        return self.x_samples[idx], idx % self.y_dim, idx

    def __len__(self):
        return len(self.x_samples)


class RandDebugDataset(Dataset):
    """Random input/output testing data"""
    def __init__(self, sample_n=8000, x_dim=1000, y_dim=10, regression=True):
        self.sample_n = sample_n
        self.x_dim = x_dim
        self.y_dim = y_dim
        if regression:
            tile_n = sample_n // y_dim
            self.x = torch.rand(y_dim*tile_n, x_dim)
            tile_y = torch.rand(y_dim, y_dim)
            self.y = torch.from_numpy(np.tile(tile_y, (tile_n, 1)))
        else:
            self.x = torch.rand(sample_n, x_dim)
            self.y = torch.randint(0, y_dim, (sample_n,)).long()

    def __getitem__(self, idx):
        return self.x[idx], self.y[idx], idx

    def __len__(self):
        return self.sample_n


class LinearTreeDataset(WordDataset):
    """Encoded sequence to word forest vectors with random attention"""
    def __init__(self, wmbed_obj, layer_idx, tree_idx, noise_stddev=0.0):
        """Layered tree based definition encodings"""
        print("DS_INIT: building dataset...", end='', flush=True)
        key_def_list = get_definitions(wmbed_obj.key_list)
        w_forest = wmbed_obj.word_forest
        x_dim = wmbed_obj.vect_len
        y_dim = w_forest.get_branch_len(layer_idx, tree_idx) + 1
        super().__init__(key_def_list, x_dim, y_dim, classification=True)
        self.noise_stddev = noise_stddev
        self.x_samples = torch.DoubleTensor(len(self), self.x_dim)
        for w_idx, (_, w_list) in enumerate(self.key_val_list):
            for w_key, w_pos in w_list:
                w_vect = wmbed_obj[w_key]
                self.x_samples[w_idx] += torch.from_numpy(w_vect)
            self.x_samples[w_idx] /= torch.max(torch.abs(self.x_samples[w_idx]))
        for w_idx, (w_key, _) in enumerate(self.key_val_list):
            branch_idx = w_forest.get_branch_index(layer_idx, tree_idx, w_key)
            self.y_tensor[w_idx] = branch_idx
        print("\rDS_INIT: operation complete.")
        self.refresh()

    def refresh(self):
        """Add noise to each sample in the dataset"""
        print("\rDS_REFRESH: updating samples... ", end='', flush=True)
        self.x_tensor[:] = self.x_samples
        x_noise = torch.randn(len(self), self.x_dim)
        x_noise *= self.noise_stddev
        self.x_tensor += x_noise
        print("\rDS_REFRESH: operation complete.\r", end='', flush=True)


class SequentialTreeDataset(WordDataset):
    """Facilitate recurrent model training. Call update() after each batch,
    call Refresh() after each epoch to simulate recurrent learning"""
    def __init__(self, wmbed_obj, layer_idx, tree_idx, attn=8, noise_stddev=0.0):
        print("DS_INIT: building dataset...", end='', flush=True)
        key_def_list = get_definitions(wmbed_obj.key_list)
        w_forest = wmbed_obj.word_forest
        x_dim = wmbed_obj.vect_len
        y_dim = w_forest.get_branch_len(layer_idx, tree_idx) + 1
        super().__init__(key_def_list, x_dim, y_dim, classification=True)
        self.attn_len = attn
        self.max_len = max([len(k_def[1]) for k_def in key_def_list])
        self.attn_indices = torch.randint(0, self.max_len, (len(self),))
        self.noise_stddev = noise_stddev
        self.wmbed_obj = wmbed_obj
        self.x_tensor = None  # Free this memory, instead use getitem
        # Initialize base state and target vectors
        for w_idx, (w_key, _) in enumerate(self.key_val_list):
            branch_idx = w_forest.get_branch_index(layer_idx, tree_idx, w_key)
            self.y_tensor[w_idx] = branch_idx
        print("\rDS_INIT: operation complete.")

    def __getitem__(self, idx):
        x_tensor = self.get_attention(idx)
        if self.noise_stddev > 0:
            x_noise = torch.randn(self.attn_len, self.x_dim)
            x_tensor += x_noise
        return x_tensor, self.y_tensor[idx], idx

    def get_attention(self, sample_idx):
        attn_tensor = torch.FloatTensor(self.attn_len, self.x_dim)
        for idx in range(self.attn_len):
            attn_list = self.key_val_list[sample_idx][1]
            attn_idx = int(self.attn_indices[sample_idx] + idx) % len(attn_list)
            w_key, w_pos = attn_list[attn_idx]
            attn_tensor[idx] = torch.from_numpy(self.wmbed_obj[w_key])
        return attn_tensor

    def refresh(self):
        """Reinitialize attention starting indices"""
        print("\rDS_REFRESH: updating samples... ", end='', flush=True)
        self.attn_indices = torch.randint(0, self.max_len, (len(self),))
        print("\rDS_REFRESH: operation complete.\r", end='', flush=True)


def get_definitions(key_list):
    """Return a tuple for all relevant key definitions"""
    def_dict = ws.get_wnet_definitions(key_list)
    # Store key word, def list tuple for each definition str
    return [(k, d) for k in key_list for d in def_dict[k]]


def get_lindef_datasets(wmbed_obj, layer_idx, tree_idx, noise_stddev):
    """Return a pair of datasets for linear definition processing"""
    test_ds = LinearTreeDataset(wmbed_obj, layer_idx, tree_idx, 0.0)
    train_ds = LinearTreeDataset(wmbed_obj, layer_idx, tree_idx, noise_stddev)
    return test_ds, train_ds


def get_seqdef_datasets(wmbed_obj, layer_idx, tree_idx, attn, noise_stddev):
    """Return a pair of datasets for sequential definition processing"""
    test_ds = SequentialTreeDataset(wmbed_obj, layer_idx, tree_idx, attn, 0.0)
    train_ds = SequentialTreeDataset(wmbed_obj, layer_idx, tree_idx, attn, noise_stddev)
    return test_ds, train_ds