"""
Word tree and tree node classes
Implements a dynamic contextual word tree and its constituent functionality
"""

from random import randint


class WordTree(object):
    """Branching key map translating raw text strings to localized nodes"""
    def __init__(self, stack_layers):
        # Layer list contains the root branch node of each key layer
        self.key_nodes = {}  # key_str: WordTreeKey nodes map
        self.root_node = WordTreeBranch('<Root>', layer_leaf=False)
        self.stack_layers = stack_layers

    def __len__(self):
        return len(self.key_nodes)

    def get_key_node(self, node_key):
        """Return a WordTreeKey node with the key node_key"""
        if node_key not in self.key_nodes:
            key_node = WordTreeKey(node_key, self.stack_layers)
            self.key_nodes[node_key] = key_node
        return self.key_nodes[node_key]

    def get_leaf(self, key_stack, p_default):
        """Return the leaf node selected from tree branch traversal"""
        parent_node = self.root_node  # Tree root node
        for l_idx in range(key_stack.key_layers):  # Traverse key layers
            layer_keys = key_stack.get_layer_keys(l_idx, include_next=False)
            parent_node = parent_node.select_leaf(layer_keys, 0, p_default)
            if not parent_node.layer_leaf:
                parent_node = parent_node.select_value()  # Next key node
        key_node = parent_node.select_value()
        return key_node.node_key

    def get_tree_dimensions(self):
        """Return a dictionary of tree descriptive features for tabulation"""
        dim_list = []  # Branch, leaf, node and max layer leaf counts
        headers = ['branches', 'leaves', 'leaf depth', 'nodes']
        self.root_node.get_dimensions(dim_list, 0, 0)
        return dim_list, headers

    def map_leaf(self, key_stack, p_default):
        """Attempt to map a probabilistic path to the target key"""
        assert key_stack.target_key in self.key_nodes
        key_column = self.key_nodes[key_stack.target_key].get_column()
        key_stack.update_next(key_column, key_stack.target_key)
        leaf_key = self.get_leaf(key_stack, p_default)
        map_flag = leaf_key == key_stack.target_key
        if map_flag:
            self.map_state(key_stack)
        return map_flag

    def map_state(self, key_stack):
        """Update branch_maps with trans_key to next_key transition edge"""
        parent_node = self.root_node  # Initialize with tree root node
        key_node = self.get_key_node(key_stack.target_key)
        key_node.update_branch(self.stack_layers, key_stack.context_key)
        for l_idx in range(key_stack.key_layers):  # Map each key layer
            layer_keys = key_stack.get_layer_keys(layer_index=l_idx)
            # Map edges between layer keys to the 'next' key value (key node)
            parent_node = parent_node.map_layer(layer_keys)
            key_node.update_branch(l_idx, parent_node.node_key)
        # Map leaf-key transition
        parent_node.map_edge(key_stack.target_key, key_node)

    def prune_tree(self, min_value):
        """Remove all node branches whose ref_count falls below min_value"""
        self.root_node.prune_branch(min_value)


class WordTreeBranch(object):
    """A tree node containing multiple outgoing key-value edges"""
    def __init__(self, node_key, layer_leaf=False):
        self.default_key = None  # Default branch edge key
        self.edge_map = dict()  # edge_key: (node, ref_count) items
        self.layer_leaf = layer_leaf
        self.node_key = node_key
        self.total_refs = 0

    def __contains__(self, edge_keys):
        return edge_keys in self.edge_map

    def __getitem__(self, edge_key):
        return self.edge_map[edge_key]

    def __len__(self):
        return len(self.edge_map)

    def __repr__(self):
        if self.layer_leaf:
            return '%s.l=%d' % (str(self.node_key), len(self.edge_map))
        return '%s.b=%d' % (str(self.node_key), len(self.edge_map))

    def get_dimensions(self, dim_list, layer_idx, leaf_idx):
        """Update dim_list at layer_idx and continue traversing child nodes"""
        leaf_idx += self.layer_leaf
        if layer_idx >= len(dim_list):  # Add layer dimension list
            dim_list.append([0, 0, 0, 0])
        dim_list[layer_idx][0] += len(self.edge_map)  # Add branch count
        dim_list[layer_idx][1] += self.layer_leaf  # Add layer leaf
        dim_list[layer_idx][3] += 1  # Add node count to layer
        if leaf_idx > dim_list[layer_idx][2]:  # Number of leaf nodes in branch
            dim_list[layer_idx][2] = leaf_idx
        for child_node, ref_count in self.edge_map.values():
            child_node.get_dimensions(dim_list, layer_idx + 1, leaf_idx)

    def map_layer(self, layer_keys, key_idx=0):
        """Map each key in layer_keys until a matching leaf node is reached"""
        if layer_keys[key_idx] in self.edge_map:  # Key already mapped to node
            next_node = self.edge_map[layer_keys[key_idx]][0]
            if next_node.layer_leaf:
                if next_node.node_key != layer_keys[-1]:
                    # Early terminating leaf; replace with branch node
                    assert key_idx < len(layer_keys) - 1
                    branch_node = WordTreeBranch(layer_keys[key_idx])
                    self.map_edge(layer_keys[key_idx], branch_node, True)
                    branch_node.map_edge(next_node.node_key, next_node)
                    return branch_node.map_leaf(layer_keys[-1])
                else:  # Return matching layer leaf node
                    self.map_edge(layer_keys[-1], next_node)
                    return next_node
            else:  # Branch node; proceed to next node
                assert next_node.node_key == layer_keys[key_idx]
                self.map_edge(layer_keys[key_idx], next_node)
                return next_node.map_layer(layer_keys, key_idx + 1)
        else:  # Key not yet mapped; map to a new node
            return self.map_leaf(layer_keys[key_idx])

    def map_leaf(self, leaf_key):
        """Map the edge from leaf_key to a new leaf branch node"""
        leaf_node = WordTreeBranch(leaf_key, layer_leaf=True)
        self.map_edge(leaf_key, leaf_node)
        return leaf_node

    def map_edge(self, edge_key, edge_node, replace_edge=False):
        """Add or update an edge from edge_key to edge_node"""
        self.total_refs += 1
        if edge_key in self.edge_map:
            if replace_edge:  # Replace existing edge
                ref_count = self.edge_map[edge_key][1] + 1
                self.edge_map[edge_key] = [edge_node, ref_count]
            else:  # Update existing edge
                self.edge_map[edge_key][1] += 1
        else:  # Map a new key-value edge
            self.edge_map[edge_key] = [edge_node, 1]
        if self.default_key is None:
            self.default_key = edge_key
        elif self.edge_map[edge_key][1] > self.edge_map[self.default_key][1]:
            self.default_key = edge_key  # Update default key

    def prune_branch(self, min_value):
        """Remove all key-value edges that do not meet min_value ref_count"""
        min_refs = self.edge_map[self.default_key][1] * min_value
        edge_items, node_queue = [e for e in self.edge_map.items()], []
        self.edge_map, self.total_refs = {}, 0  # Reset branch values
        for edge_key, edge_value in edge_items:
            if edge_value[1] >= min_refs:  # Add edges meeting min_refs
                self.edge_map[edge_key] = edge_value
                self.total_refs += edge_value[1]
                if edge_value[0].edge_map is not None:  # Traverse child nodes
                    node_queue.append(edge_value[0])  # Append branch nodes
        for edge_node in node_queue:  # Recursive call to child node
            edge_node.prune_branch(min_value)

    def select_leaf(self, branch_keys, k_idx=0, prob_default=True):
        """Return the branch node selected from layer_keys"""
        if branch_keys[k_idx] in self.edge_map:
            next_node = self.edge_map[branch_keys[k_idx]][0]
        else:  # No matching key, select default or prob. edge
            if prob_default:  # Select edge based on ref_count probability
                next_node = self.select_value()
            else:  # Select default edge (highest ref_count)
                next_node = self.edge_map[self.default_key][0]
        if next_node.layer_leaf or k_idx == len(branch_keys) - 1:
            return next_node
        return next_node.select_leaf(branch_keys, k_idx + 1, prob_default)

    def select_value(self):
        """Return an edge value selected by its ref count probability"""
        if len(self.edge_map) == 1:  # Return the only branch in this node
            return self.edge_map[self.default_key][0]  # Direct map
        rand_val, ref_sum = randint(0, self.total_refs - 1), 0
        for branch_item in self.edge_map.values():  # Select leaf probabilistically
            ref_sum += branch_item[1]
            if ref_sum >= rand_val:  # P-value reached, select this leaf
                p_val = (branch_item[1] / self.total_refs) * 100
                return branch_item[0]
        raise Exception("rand_val {} exceeds ref_sum".format(rand_val))


class WordTreeKey(object):
    """Two way leaf-branch map between tree key layers"""
    def __init__(self, node_key, stack_layers):
        self.edge_count = 0
        self.node_key = node_key
        self.branch_nodes = [WordTreeBranch(i) for i in range(stack_layers + 1)]

    def __len__(self):
        return self.edge_count

    def __repr__(self):
        return '%s.b=%d' % (str(self.node_key), self.edge_count)

    def get_column(self):
        column_keys = tuple()
        for n_idx in range(len(self.branch_nodes) - 1):
            column_keys += (self.branch_nodes[n_idx].select_value(),)
        return column_keys

    def get_dimensions(self, dim_list, layer_idx, leaf_idx):
        pass

    def update_branch(self, node_idx, edge_key):
        branch_node = self.branch_nodes[node_idx]
        if edge_key not in branch_node:  # Edge already exists
            self.edge_count += 1
        branch_node.map_edge(edge_key, edge_key)
