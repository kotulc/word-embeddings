"""
A collection of logging functionality for Word model
"""

from datetime import datetime
import os


class WordLogs(object):
    """Log file object representing rec/int streams"""
    def __init__(self, max_entries):
        self.log_buffer = ['']
        self.line_count = 0
        self.max_entries = max_entries

    def log_entry(self, log_str, end_line=False):
        """Add a new line to the log file"""
        if self.line_count < self.max_entries:
            time_str = '%-6d [' + datetime.now().strftime("%H:%M:%S") + ']: '
            log_str = time_str % (self.line_count,) + log_str
            self.log_buffer[self.line_count] += log_str
            if end_line:
                self.log_buffer[self.line_count] += '\n'
                self.log_buffer.append('')
                self.line_count += 1

    def log_sample(self, context, sample):
        """Add a sample entry to the log file"""
        if self.line_count < self.max_entries:
            time_str = '\n%-6d [' + datetime.now().strftime("%H:%M:%S") + ']: '
            log_str = time_str + 'sample - %s: %s\n'
            log_str = log_str % (self.line_count, context, sample)
            self.log_buffer[self.line_count] += log_str
            self.log_buffer.append('')
            self.line_count += 1

    def log_update(self, log_str, end_line=False):
        """Update the current line of the log file"""
        if self.line_count < self.max_entries:
            self.log_buffer[self.line_count] += log_str
            if end_line:
                self.log_buffer[self.line_count] += '\n'
                self.log_buffer.append('')
                self.line_count += 1

    def save_log(self, log_path, flush_buffer=True):
        """Save buffer data to the log file at the specified path"""
        print("SAVE_LOG: saving log buffer...", end='')
        if not os.path.exists(log_path):
            os.mkdir(log_path)
        log_file = open(log_path, "w+")
        log_file.writelines(self.log_buffer)
        log_file.close()
        if flush_buffer:
            self.log_buffer = ['']
            self.line_count = 0
        print("\rSAVE_LOG: operation complete.")
